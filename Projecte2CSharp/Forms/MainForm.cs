﻿using Projecte2CSharp.Clases;
using Projecte2CSharp.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.Forms
{
    public partial class MainForm : Form
    {
        public List<Button> buttons = new List<Button>();
        public int currentTab { get; set; }
        public static usuaris user { get; set; }
        public List<UserControl> controls = new List<UserControl>();
        

        public MainForm(usuaris user)
        {
            InitializeComponent();
            controls.Add(entitatsControls1);
            controls.Add(instalacionsControl1);
            controls.Add(equipsControl);
            controls.Add(configControl);
            buttons.Add(this.buttonInstalacions);
            buttons.Add(this.buttonEntitats);
            buttons.Add(this.buttonDemandes);
            buttons.Add(this.buttonEquips);
            buttons.Add(this.buttonConfig);
            MainForm.user = user;
            configControl.user = user;
        }

        

        private void MainForm_Load(object sender, EventArgs e)
        {
            activateFirstTab();
            
        }

        private void buttonEntitats_Click(object sender, EventArgs e)
        {
            activateFirstTab();
        }

        private void buttonInstalacions_Click(object sender, EventArgs e)
        {
            activateSecondTab();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            activateThirdTab();
        }

        private void buttonEquips_Click_1(object sender, EventArgs e)
        {
            activateFourthTab();
        }

        private void buttonConfig_Click(object sender, EventArgs e)
        {
            activateConfigTab();
        }



        //Funcions
        public void setButtonsColor(Button btn)
        {
            foreach(Button b in buttons)
            {
                b.BackColor = Color.FromArgb(88, 89, 91);
            }
            btn.BackColor = Color.FromArgb(105, 106, 109);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MainForm_Activated(object sender, EventArgs e)
        {

            switch (currentTab)
            {
                case 1:
                    activateFirstTab();
                    break;
                case 2:
                    activateSecondTab();
                    break;
                case 3:
                    activateThirdTab();
                    break;
                case 4:
                    activateFourthTab();
                    break;
                case 5:
                    activateConfigTab();
                    break;
                default:
                    activateFirstTab();
                    break;

            }
        }

        //region TAB FUNCTIONS

        public void activateFirstTab()
        {
            currentTab = 1;
            entitatsControls1.BringToFront();
            sidePanel.Location = new Point(0, buttonEntitats.Location.Y);
            setButtonsColor(buttonEntitats);
            entitatsControls1.getEntitats();
            enableTab(entitatsControls1);
            entitatsControls1.Focus();
        }

        public void activateSecondTab()
        {
            currentTab = 2;
            instalacionsControl1.BringToFront();
            sidePanel.Location = new Point(0, buttonInstalacions.Location.Y);
            setButtonsColor(buttonInstalacions);
            instalacionsControl1.getInstalacions();
            enableTab(instalacionsControl1);
            instalacionsControl1.Focus();
        }

        public void activateThirdTab()
        {
            currentTab = 3;
            demandesControl1.BringToFront();
            sidePanel.Location = new Point(0, buttonDemandes.Location.Y);
            setButtonsColor(buttonDemandes);
            demandesControl1.Focus();
            enableTab(demandesControl1);
            
            
            

        }

        public void activateFourthTab()
        {
            currentTab = 4;
            equipsControl.BringToFront();
            sidePanel.Location = new Point(0, buttonEquips.Location.Y);
            setButtonsColor(buttonEquips);
            equipsControl.getEquips();
            enableTab(equipsControl);
            equipsControl.Focus();
        }

        public void activateConfigTab()
        {
            currentTab = 5;
            configControl.BringToFront();
            sidePanel.Location = new Point(0, buttonConfig.Location.Y);
            setButtonsColor(buttonConfig);
            configControl.Focus();
            enableTab(configControl);
            configControl.getData();

            if (!(bool)user.superadmin)
            {
                configControl.enablePasswordChange();
            }
        }

        

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        public void enableTab(UserControl control)
        {
            foreach(UserControl c in controls)
            {
                c.Enabled = false;
            }
            control.Enabled = true;
        }

        public void disableAllTabs()
        {
            foreach(UserControl c in controls)
            {
                c.Enabled = false;
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            about a = new about();
            a.ShowDialog();
        }



        //endregion


    }
}
