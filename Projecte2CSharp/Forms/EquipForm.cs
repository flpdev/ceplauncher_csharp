﻿using Projecte2CSharp.Clases.POJO;
using Projecte2CSharp.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.Forms
{
    public partial class EquipForm : Form
    {
        public equips equip { get; set; }
        public Boolean isModification { get; set; }
        public int id_entitat { get; set; }

        private entitats entitat;

        public EquipForm(entitats entitat)
        {
            InitializeComponent();
            this.entitat = entitat;
        }

        public EquipForm()
        {
            InitializeComponent();
           
        }

        public EquipForm(equips equip)
        {
            InitializeComponent();
            this.isModification = true;
            this.equip = equip;

        }
        private void EquipForm_Load(object sender, EventArgs e)
        {
            string msg = "";
            bindingSourceCategories.DataSource = ORMCategories.selectCategories(ref msg);
            bindingSourceSexes.DataSource = ORMSexes.selectSexes(ref msg);
            bindingSourceEspais.DataSource = ORMEspais.selectEspais(ref msg);
            bindingSourceCompeticions.DataSource = ORMCompeticio.selectCompeticions(ref msg);
            bindingSourceEntitats.DataSource = ORMEntitats.selectEntitats(ref msg);
            bindingSourceEsports.DataSource = ORMEsports.selectEsports(ref msg);
            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            //Modificacio
            if (isModification){
                bindingSourceActivitatsProgramades.DataSource = ActivitatProgramada.createDemandesByEquip(ORMActivitatConcedida.selectActivitatConcedidaByEquip(equip.id, ref msg));
                ActivitatEquip ae = new ActivitatEquip(equip.id);
                List<HorariComplet> horaris = ae.getActivitaFromBD();
                bindingSourceHorarisComplets.DataSource = horaris;
                this.Text = equip.nom;
                comboBoxEntitat.SelectedValue = equip.id_entitat;
                comboBoxEntitat.Enabled = false;
                textBoxNomEquip.Text = equip.nom;
                comboBoxCompeticions.SelectedValue = equip.id_competicio;
                comboBoxCategoria.SelectedValue = equip.id_categoria;
                comboBoxSexe.SelectedValue = equip.id_sexe;
                comboBoxEsport.SelectedValue = equip.id_esport;

                string msg2 = "";
                bindingSourceHoraris.DataSource = ORMHoraris_activitats.selectHorarisActivitats(ref msg2);
                if (!msg2.Equals(""))
                {
                    MessageBox.Show(msg2, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            //Nou equip desde entitat
            else if(id_entitat != 0){
                this.Text = "Nou equip";
                labelNomEquip.Text = "Nou equip";
                comboBoxEntitat.SelectedValue = id_entitat;
                comboBoxEntitat.Enabled = false;
            }
            //Nou equip desde MENU EQUIPS
            else
            {
                this.Text = "Nou equip";
                labelNomEquip.Text = "Nou equip";
            }
            
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBoxNomEquip_TextChanged(object sender, EventArgs e)
        {
            labelNomEquip.Text = textBoxNomEquip.Text;
        }

        private void buttonSaveChanges_Click(object sender, EventArgs e)
        {
            if (isModification)
            {
                equip.nom = textBoxNomEquip.Text;
                equip.id_categoria = (int)comboBoxCategoria.SelectedValue;
                equip.id_sexe = (int)comboBoxSexe.SelectedValue;
                equip.id_competicio = (int)comboBoxCompeticions.SelectedValue;
                equip.id_esport = (int)comboBoxEsport.SelectedValue;
                string msg = ORMEquips.updateEquip(equip);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else if(id_entitat != 0)
            {
                equip = new equips();
                equip.id_entitat = id_entitat;
                equip.nom = textBoxNomEquip.Text;
                equip.id_categoria = (int)comboBoxCategoria.SelectedValue;
                equip.id_sexe = (int)comboBoxSexe.SelectedValue;
                equip.id_competicio = (int)comboBoxCompeticions.SelectedValue;
                equip.id_esport = (int)comboBoxEsport.SelectedValue;
                string msg = ORMEquips.insertEquip(equip);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                equip = new equips();
                equip.nom = textBoxNomEquip.Text;
                equip.id_entitat = (int)comboBoxEntitat.SelectedValue;
                equip.id_categoria = (int)comboBoxCategoria.SelectedValue;
                equip.id_sexe = (int)comboBoxSexe.SelectedValue;
                equip.id_competicio = (int)comboBoxCompeticions.SelectedValue;
                equip.id_esport = (int)comboBoxEsport.SelectedValue;
                entitat.equips.Add(equip);
            }
            this.Close();
        }
    }
}
