﻿namespace Projecte2CSharp.Forms
{
    partial class FormDemanda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                //TUvimos que comentar esta linia sino no funcionaba
                //components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDemanda));
            this.label1 = new System.Windows.Forms.Label();
            this.labelNumHores = new System.Windows.Forms.Label();
            this.comboBoxInstalacions = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxEspais = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.listBoxHoraris = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dataGridViewHoraris = new System.Windows.Forms.DataGridView();
            this.id_dia_setmana = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.hora_inici = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hora_final = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.assignatString = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonAssignar = new System.Windows.Forms.Button();
            this.labelAssignacio = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxHorariFi = new System.Windows.Forms.ComboBox();
            this.comboBoxHorariInici = new System.Windows.Forms.ComboBox();
            this.labelEntitat = new System.Windows.Forms.Label();
            this.labelTipus = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.buttonDeleteHorari = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonCancelar = new System.Windows.Forms.Button();
            this.bindingSourceEspais = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceInstalacions = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceDies = new System.Windows.Forms.BindingSource(this.components);
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSourceHoraris = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHoraris)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEspais)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceInstalacions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceDies)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceHoraris)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 25);
            this.label1.TabIndex = 31;
            this.label1.Text = "Hores demandades: ";
            // 
            // labelNumHores
            // 
            this.labelNumHores.AutoSize = true;
            this.labelNumHores.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumHores.Location = new System.Drawing.Point(181, 74);
            this.labelNumHores.Name = "labelNumHores";
            this.labelNumHores.Size = new System.Drawing.Size(102, 25);
            this.labelNumHores.TabIndex = 32;
            this.labelNumHores.Text = "numHores";
            // 
            // comboBoxInstalacions
            // 
            this.comboBoxInstalacions.DataSource = this.bindingSourceInstalacions;
            this.comboBoxInstalacions.DisplayMember = "nom";
            this.comboBoxInstalacions.FormattingEnabled = true;
            this.comboBoxInstalacions.Location = new System.Drawing.Point(130, 460);
            this.comboBoxInstalacions.Name = "comboBoxInstalacions";
            this.comboBoxInstalacions.Size = new System.Drawing.Size(246, 33);
            this.comboBoxInstalacions.TabIndex = 33;
            this.comboBoxInstalacions.ValueMember = "id";
            this.comboBoxInstalacions.SelectedIndexChanged += new System.EventHandler(this.comboBoxInstalacions_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 463);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 25);
            this.label2.TabIndex = 34;
            this.label2.Text = "Instal·lació";
            // 
            // comboBoxEspais
            // 
            this.comboBoxEspais.DataSource = this.bindingSourceEspais;
            this.comboBoxEspais.DisplayMember = "nom";
            this.comboBoxEspais.FormattingEnabled = true;
            this.comboBoxEspais.Location = new System.Drawing.Point(130, 508);
            this.comboBoxEspais.Name = "comboBoxEspais";
            this.comboBoxEspais.Size = new System.Drawing.Size(246, 33);
            this.comboBoxEspais.TabIndex = 35;
            this.comboBoxEspais.ValueMember = "id";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 511);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 25);
            this.label3.TabIndex = 36;
            this.label3.Text = "Espai";
            // 
            // listBoxHoraris
            // 
            this.listBoxHoraris.FormattingEnabled = true;
            this.listBoxHoraris.ItemHeight = 25;
            this.listBoxHoraris.Location = new System.Drawing.Point(464, 205);
            this.listBoxHoraris.Name = "listBoxHoraris";
            this.listBoxHoraris.Size = new System.Drawing.Size(236, 229);
            this.listBoxHoraris.TabIndex = 37;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(136, 25);
            this.label4.TabIndex = 38;
            this.label4.Text = "Dies sol·licitats";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(459, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(160, 25);
            this.label5.TabIndex = 39;
            this.label5.Text = "Horari instal·lació";
            // 
            // dataGridViewHoraris
            // 
            this.dataGridViewHoraris.AllowUserToAddRows = false;
            this.dataGridViewHoraris.AllowUserToResizeColumns = false;
            this.dataGridViewHoraris.AllowUserToResizeRows = false;
            this.dataGridViewHoraris.AutoGenerateColumns = false;
            this.dataGridViewHoraris.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewHoraris.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewHoraris.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridViewHoraris.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewHoraris.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewHoraris.ColumnHeadersHeight = 35;
            this.dataGridViewHoraris.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewHoraris.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_dia_setmana,
            this.hora_inici,
            this.hora_final,
            this.assignatString,
            this.idDataGridViewTextBoxColumn});
            this.dataGridViewHoraris.DataSource = this.bindingSourceHoraris;
            this.dataGridViewHoraris.EnableHeadersVisualStyles = false;
            this.dataGridViewHoraris.Location = new System.Drawing.Point(31, 205);
            this.dataGridViewHoraris.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dataGridViewHoraris.MultiSelect = false;
            this.dataGridViewHoraris.Name = "dataGridViewHoraris";
            this.dataGridViewHoraris.ReadOnly = true;
            this.dataGridViewHoraris.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewHoraris.RowHeadersVisible = false;
            this.dataGridViewHoraris.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewHoraris.RowTemplate.Height = 35;
            this.dataGridViewHoraris.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewHoraris.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridViewHoraris.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewHoraris.Size = new System.Drawing.Size(392, 237);
            this.dataGridViewHoraris.TabIndex = 30;
            this.dataGridViewHoraris.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewHoraris_CellClick);
            this.dataGridViewHoraris.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewHoraris_CellContentClick);
            this.dataGridViewHoraris.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewHoraris_CellMouseClick);
            // 
            // id_dia_setmana
            // 
            this.id_dia_setmana.DataPropertyName = "id_dia_setmana";
            this.id_dia_setmana.DataSource = this.bindingSourceDies;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.id_dia_setmana.DefaultCellStyle = dataGridViewCellStyle2;
            this.id_dia_setmana.DisplayMember = "dia";
            this.id_dia_setmana.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.id_dia_setmana.HeaderText = "Dia ";
            this.id_dia_setmana.Name = "id_dia_setmana";
            this.id_dia_setmana.ReadOnly = true;
            this.id_dia_setmana.ValueMember = "id";
            // 
            // hora_inici
            // 
            this.hora_inici.DataPropertyName = "hora_inici";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.hora_inici.DefaultCellStyle = dataGridViewCellStyle3;
            this.hora_inici.HeaderText = "Inici";
            this.hora_inici.Name = "hora_inici";
            this.hora_inici.ReadOnly = true;
            // 
            // hora_final
            // 
            this.hora_final.DataPropertyName = "hora_final";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.hora_final.DefaultCellStyle = dataGridViewCellStyle4;
            this.hora_final.HeaderText = "Fi";
            this.hora_final.Name = "hora_final";
            this.hora_final.ReadOnly = true;
            // 
            // assignatString
            // 
            this.assignatString.DataPropertyName = "assignatString";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.assignatString.DefaultCellStyle = dataGridViewCellStyle5;
            this.assignatString.HeaderText = "Assignat";
            this.assignatString.Name = "assignatString";
            this.assignatString.ReadOnly = true;
            // 
            // buttonAssignar
            // 
            this.buttonAssignar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.buttonAssignar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.buttonAssignar.FlatAppearance.BorderSize = 3;
            this.buttonAssignar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAssignar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonAssignar.Location = new System.Drawing.Point(25, 573);
            this.buttonAssignar.Name = "buttonAssignar";
            this.buttonAssignar.Size = new System.Drawing.Size(186, 41);
            this.buttonAssignar.TabIndex = 40;
            this.buttonAssignar.Text = "Assignar activitat";
            this.buttonAssignar.UseVisualStyleBackColor = false;
            this.buttonAssignar.Click += new System.EventHandler(this.buttonAssignar_Click);
            // 
            // labelAssignacio
            // 
            this.labelAssignacio.AutoSize = true;
            this.labelAssignacio.Location = new System.Drawing.Point(413, 485);
            this.labelAssignacio.Name = "labelAssignacio";
            this.labelAssignacio.Size = new System.Drawing.Size(105, 25);
            this.labelAssignacio.TabIndex = 41;
            this.labelAssignacio.Text = "Assignació:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(537, 463);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 25);
            this.label7.TabIndex = 50;
            this.label7.Text = "De";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(537, 508);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 25);
            this.label6.TabIndex = 49;
            this.label6.Text = "a";
            // 
            // comboBoxHorariFi
            // 
            this.comboBoxHorariFi.FormattingEnabled = true;
            this.comboBoxHorariFi.Location = new System.Drawing.Point(570, 505);
            this.comboBoxHorariFi.Name = "comboBoxHorariFi";
            this.comboBoxHorariFi.Size = new System.Drawing.Size(130, 33);
            this.comboBoxHorariFi.TabIndex = 48;
            this.comboBoxHorariFi.SelectedIndexChanged += new System.EventHandler(this.comboBoxHorariFi_SelectedIndexChanged);
            // 
            // comboBoxHorariInici
            // 
            this.comboBoxHorariInici.FormattingEnabled = true;
            this.comboBoxHorariInici.Location = new System.Drawing.Point(570, 460);
            this.comboBoxHorariInici.Name = "comboBoxHorariInici";
            this.comboBoxHorariInici.Size = new System.Drawing.Size(130, 33);
            this.comboBoxHorariInici.TabIndex = 47;
            this.comboBoxHorariInici.SelectedIndexChanged += new System.EventHandler(this.comboBoxHorariInici_SelectedIndexChanged);
            // 
            // labelEntitat
            // 
            this.labelEntitat.AutoSize = true;
            this.labelEntitat.Font = new System.Drawing.Font("Source Sans Pro", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEntitat.Location = new System.Drawing.Point(26, 23);
            this.labelEntitat.Name = "labelEntitat";
            this.labelEntitat.Size = new System.Drawing.Size(61, 38);
            this.labelEntitat.TabIndex = 51;
            this.labelEntitat.Text = "Ent";
            // 
            // labelTipus
            // 
            this.labelTipus.AutoSize = true;
            this.labelTipus.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTipus.Location = new System.Drawing.Point(90, 102);
            this.labelTipus.Name = "labelTipus";
            this.labelTipus.Size = new System.Drawing.Size(57, 25);
            this.labelTipus.TabIndex = 53;
            this.labelTipus.Text = "tipus";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(29, 102);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 25);
            this.label9.TabIndex = 52;
            this.label9.Text = "Tipus";
            // 
            // buttonDeleteHorari
            // 
            this.buttonDeleteHorari.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(181)))));
            this.buttonDeleteHorari.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonDeleteHorari.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonDeleteHorari.FlatAppearance.BorderSize = 3;
            this.buttonDeleteHorari.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDeleteHorari.Location = new System.Drawing.Point(25, 620);
            this.buttonDeleteHorari.Name = "buttonDeleteHorari";
            this.buttonDeleteHorari.Size = new System.Drawing.Size(186, 41);
            this.buttonDeleteHorari.TabIndex = 56;
            this.buttonDeleteHorari.Text = "Eliminar assignació";
            this.buttonDeleteHorari.UseVisualStyleBackColor = false;
            this.buttonDeleteHorari.Click += new System.EventHandler(this.buttonDeleteHorari_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.buttonSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.buttonSave.FlatAppearance.BorderSize = 3;
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSave.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonSave.Location = new System.Drawing.Point(514, 609);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(186, 52);
            this.buttonSave.TabIndex = 57;
            this.buttonSave.Text = "Guardar canvis";
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonCancelar
            // 
            this.buttonCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(181)))));
            this.buttonCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancelar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonCancelar.FlatAppearance.BorderSize = 3;
            this.buttonCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancelar.Location = new System.Drawing.Point(389, 610);
            this.buttonCancelar.Name = "buttonCancelar";
            this.buttonCancelar.Size = new System.Drawing.Size(119, 51);
            this.buttonCancelar.TabIndex = 58;
            this.buttonCancelar.Text = "Cancel·lar";
            this.buttonCancelar.UseVisualStyleBackColor = false;
            this.buttonCancelar.Click += new System.EventHandler(this.buttonCancelar_Click);
            // 
            // bindingSourceEspais
            // 
            this.bindingSourceEspais.DataSource = typeof(Projecte2CSharp.espais);
            // 
            // bindingSourceInstalacions
            // 
            this.bindingSourceInstalacions.DataSource = typeof(Projecte2CSharp.instalacions);
            // 
            // bindingSourceDies
            // 
            this.bindingSourceDies.DataSource = typeof(Projecte2CSharp.dies_setmana);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // bindingSourceHoraris
            // 
            this.bindingSourceHoraris.DataSource = typeof(Projecte2CSharp.horaris_adients);
            // 
            // FormDemanda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(727, 679);
            this.Controls.Add(this.buttonCancelar);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonDeleteHorari);
            this.Controls.Add(this.labelTipus);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.labelEntitat);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.comboBoxHorariFi);
            this.Controls.Add(this.comboBoxHorariInici);
            this.Controls.Add(this.labelAssignacio);
            this.Controls.Add(this.buttonAssignar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.listBoxHoraris);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBoxEspais);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxInstalacions);
            this.Controls.Add(this.labelNumHores);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridViewHoraris);
            this.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "FormDemanda";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Demanda";
            this.Load += new System.EventHandler(this.FormDemanda_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHoraris)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEspais)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceInstalacions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceDies)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceHoraris)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource bindingSourceHoraris;
        private System.Windows.Forms.BindingSource bindingSourceDies;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelNumHores;
        private System.Windows.Forms.ComboBox comboBoxInstalacions;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxEspais;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.BindingSource bindingSourceInstalacions;
        private System.Windows.Forms.BindingSource bindingSourceEspais;
        private System.Windows.Forms.ListBox listBoxHoraris;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dataGridViewHoraris;
        private System.Windows.Forms.Button buttonAssignar;
        private System.Windows.Forms.Label labelAssignacio;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxHorariFi;
        private System.Windows.Forms.ComboBox comboBoxHorariInici;
        private System.Windows.Forms.Label labelEntitat;
        private System.Windows.Forms.Label labelTipus;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridViewComboBoxColumn id_dia_setmana;
        private System.Windows.Forms.DataGridViewTextBoxColumn hora_inici;
        private System.Windows.Forms.DataGridViewTextBoxColumn hora_final;
        private System.Windows.Forms.DataGridViewTextBoxColumn assignatString;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button buttonDeleteHorari;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonCancelar;
    }
}