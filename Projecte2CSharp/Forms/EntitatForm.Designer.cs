﻿namespace Projecte2CSharp.Forms
{
    partial class EntitatForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EntitatForm));
            this.textBoxNom = new System.Windows.Forms.TextBox();
            this.textBoxCIF = new System.Windows.Forms.TextBox();
            this.textBoxDireccio = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelNomEntitat = new System.Windows.Forms.Label();
            this.buttonSaveChanges = new System.Windows.Forms.Button();
            this.buttonCancelar = new System.Windows.Forms.Button();
            this.textBoxCorreu = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.dataGridViewTelefons = new System.Windows.Forms.DataGridView();
            this.telefonsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dataGridViewEquips = new System.Windows.Forms.DataGridView();
            this.bindingSourceEsports = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceCategories = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceSexes = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceCompeticio = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceEquips = new System.Windows.Forms.BindingSource(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.buttonEliminarTelefon = new System.Windows.Forms.Button();
            this.buttonEliminarEquip = new System.Windows.Forms.Button();
            this.buttonEditTelefon = new System.Windows.Forms.Button();
            this.buttonEditEquip = new System.Windows.Forms.Button();
            this.buttonAddTelefon = new System.Windows.Forms.Button();
            this.buttonAddEquip = new System.Windows.Forms.Button();
            this.buttonGeneratPassword = new System.Windows.Forms.Button();
            this.textBoxBuscar = new System.Windows.Forms.TextBox();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.identitatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.raoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.telefonDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_esport = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.id_categoria = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.id_sexe = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.id_competicio = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTelefons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telefonsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEquips)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEsports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCategories)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceSexes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCompeticio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEquips)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxNom
            // 
            this.textBoxNom.Location = new System.Drawing.Point(115, 73);
            this.textBoxNom.Name = "textBoxNom";
            this.textBoxNom.Size = new System.Drawing.Size(280, 28);
            this.textBoxNom.TabIndex = 0;
            this.textBoxNom.TextChanged += new System.EventHandler(this.textBoxNom_TextChanged);
            // 
            // textBoxCIF
            // 
            this.textBoxCIF.Location = new System.Drawing.Point(115, 121);
            this.textBoxCIF.Name = "textBoxCIF";
            this.textBoxCIF.Size = new System.Drawing.Size(280, 28);
            this.textBoxCIF.TabIndex = 1;
            this.textBoxCIF.TextChanged += new System.EventHandler(this.textBoxCIF_TextChanged);
            // 
            // textBoxDireccio
            // 
            this.textBoxDireccio.Location = new System.Drawing.Point(115, 170);
            this.textBoxDireccio.Name = "textBoxDireccio";
            this.textBoxDireccio.Size = new System.Drawing.Size(280, 28);
            this.textBoxDireccio.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Nom*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "CIF*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 173);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Adreça*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Correu*";
            // 
            // labelNomEntitat
            // 
            this.labelNomEntitat.AutoSize = true;
            this.labelNomEntitat.Font = new System.Drawing.Font("Source Sans Pro", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNomEntitat.Location = new System.Drawing.Point(20, 25);
            this.labelNomEntitat.Name = "labelNomEntitat";
            this.labelNomEntitat.Size = new System.Drawing.Size(80, 31);
            this.labelNomEntitat.TabIndex = 8;
            this.labelNomEntitat.Text = "label5";
            // 
            // buttonSaveChanges
            // 
            this.buttonSaveChanges.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.buttonSaveChanges.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.buttonSaveChanges.FlatAppearance.BorderSize = 3;
            this.buttonSaveChanges.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSaveChanges.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonSaveChanges.Location = new System.Drawing.Point(585, 681);
            this.buttonSaveChanges.Name = "buttonSaveChanges";
            this.buttonSaveChanges.Size = new System.Drawing.Size(165, 51);
            this.buttonSaveChanges.TabIndex = 9;
            this.buttonSaveChanges.Text = "Guardar canvis";
            this.buttonSaveChanges.UseVisualStyleBackColor = false;
            this.buttonSaveChanges.Click += new System.EventHandler(this.buttonSaveChanges_Click);
            // 
            // buttonCancelar
            // 
            this.buttonCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(181)))));
            this.buttonCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancelar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonCancelar.FlatAppearance.BorderSize = 3;
            this.buttonCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancelar.Location = new System.Drawing.Point(455, 681);
            this.buttonCancelar.Name = "buttonCancelar";
            this.buttonCancelar.Size = new System.Drawing.Size(106, 51);
            this.buttonCancelar.TabIndex = 10;
            this.buttonCancelar.Text = "Cancel·lar";
            this.buttonCancelar.UseVisualStyleBackColor = false;
            this.buttonCancelar.Click += new System.EventHandler(this.buttonCancelar_Click);
            // 
            // textBoxCorreu
            // 
            this.textBoxCorreu.Location = new System.Drawing.Point(115, 220);
            this.textBoxCorreu.Name = "textBoxCorreu";
            this.textBoxCorreu.Size = new System.Drawing.Size(280, 28);
            this.textBoxCorreu.TabIndex = 11;
            this.textBoxCorreu.TextChanged += new System.EventHandler(this.textBoxCorreu_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 270);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 20);
            this.label5.TabIndex = 12;
            this.label5.Text = "Contrasenya*";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(115, 267);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(247, 28);
            this.textBoxPassword.TabIndex = 13;
            // 
            // dataGridViewTelefons
            // 
            this.dataGridViewTelefons.AllowUserToAddRows = false;
            this.dataGridViewTelefons.AllowUserToResizeColumns = false;
            this.dataGridViewTelefons.AllowUserToResizeRows = false;
            this.dataGridViewTelefons.AutoGenerateColumns = false;
            this.dataGridViewTelefons.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewTelefons.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewTelefons.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridViewTelefons.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTelefons.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTelefons.ColumnHeadersHeight = 35;
            this.dataGridViewTelefons.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.identitatDataGridViewTextBoxColumn,
            this.raoDataGridViewTextBoxColumn,
            this.telefonDataGridViewTextBoxColumn});
            this.dataGridViewTelefons.DataSource = this.telefonsBindingSource;
            this.dataGridViewTelefons.EnableHeadersVisualStyles = false;
            this.dataGridViewTelefons.Location = new System.Drawing.Point(425, 113);
            this.dataGridViewTelefons.MultiSelect = false;
            this.dataGridViewTelefons.Name = "dataGridViewTelefons";
            this.dataGridViewTelefons.ReadOnly = true;
            this.dataGridViewTelefons.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewTelefons.RowHeadersVisible = false;
            this.dataGridViewTelefons.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewTelefons.RowTemplate.Height = 24;
            this.dataGridViewTelefons.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridViewTelefons.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewTelefons.Size = new System.Drawing.Size(325, 187);
            this.dataGridViewTelefons.TabIndex = 17;
            this.dataGridViewTelefons.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridViewTelefons_UserDeletingRow);
            this.dataGridViewTelefons.DoubleClick += new System.EventHandler(this.dataGridViewTelefons_DoubleClick);
            // 
            // telefonsBindingSource
            // 
            this.telefonsBindingSource.DataSource = typeof(Projecte2CSharp.telefons);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(428, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 20);
            this.label6.TabIndex = 18;
            this.label6.Text = "Telèfons:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 338);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 20);
            this.label7.TabIndex = 19;
            this.label7.Text = "Equips";
            // 
            // dataGridViewEquips
            // 
            this.dataGridViewEquips.AllowUserToAddRows = false;
            this.dataGridViewEquips.AllowUserToResizeColumns = false;
            this.dataGridViewEquips.AllowUserToResizeRows = false;
            this.dataGridViewEquips.AutoGenerateColumns = false;
            this.dataGridViewEquips.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewEquips.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewEquips.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridViewEquips.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewEquips.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewEquips.ColumnHeadersHeight = 35;
            this.dataGridViewEquips.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nom,
            this.dataGridViewTextBoxColumn1,
            this.id_esport,
            this.id_categoria,
            this.id_sexe,
            this.id_competicio,
            this.dataGridViewTextBoxColumn2});
            this.dataGridViewEquips.DataSource = this.bindingSourceEquips;
            this.dataGridViewEquips.EnableHeadersVisualStyles = false;
            this.dataGridViewEquips.Location = new System.Drawing.Point(25, 381);
            this.dataGridViewEquips.MultiSelect = false;
            this.dataGridViewEquips.Name = "dataGridViewEquips";
            this.dataGridViewEquips.ReadOnly = true;
            this.dataGridViewEquips.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewEquips.RowHeadersVisible = false;
            this.dataGridViewEquips.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewEquips.RowTemplate.Height = 24;
            this.dataGridViewEquips.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridViewEquips.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewEquips.Size = new System.Drawing.Size(725, 275);
            this.dataGridViewEquips.TabIndex = 20;
            this.dataGridViewEquips.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridViewEquips_UserDeletingRow);
            this.dataGridViewEquips.DoubleClick += new System.EventHandler(this.dataGridViewEquips_DoubleClick_1);
            // 
            // bindingSourceEsports
            // 
            this.bindingSourceEsports.DataSource = typeof(Projecte2CSharp.esports);
            // 
            // bindingSourceCategories
            // 
            this.bindingSourceCategories.DataSource = typeof(Projecte2CSharp.categories);
            // 
            // bindingSourceSexes
            // 
            this.bindingSourceSexes.DataSource = typeof(Projecte2CSharp.sexes);
            // 
            // bindingSourceCompeticio
            // 
            this.bindingSourceCompeticio.DataSource = typeof(Projecte2CSharp.competicions);
            // 
            // bindingSourceEquips
            // 
            this.bindingSourceEquips.DataSource = typeof(Projecte2CSharp.equips);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = global::Projecte2CSharp.Properties.Resources.search;
            this.button1.Location = new System.Drawing.Point(233, 334);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(33, 33);
            this.button1.TabIndex = 28;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // buttonEliminarTelefon
            // 
            this.buttonEliminarTelefon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(181)))));
            this.buttonEliminarTelefon.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonEliminarTelefon.FlatAppearance.BorderSize = 3;
            this.buttonEliminarTelefon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEliminarTelefon.Image = global::Projecte2CSharp.Properties.Resources.trash16;
            this.buttonEliminarTelefon.Location = new System.Drawing.Point(588, 68);
            this.buttonEliminarTelefon.Name = "buttonEliminarTelefon";
            this.buttonEliminarTelefon.Size = new System.Drawing.Size(33, 33);
            this.buttonEliminarTelefon.TabIndex = 27;
            this.buttonEliminarTelefon.UseVisualStyleBackColor = false;
            this.buttonEliminarTelefon.Click += new System.EventHandler(this.buttonEliminarTelefon_Click);
            // 
            // buttonEliminarEquip
            // 
            this.buttonEliminarEquip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(181)))));
            this.buttonEliminarEquip.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonEliminarEquip.FlatAppearance.BorderSize = 3;
            this.buttonEliminarEquip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEliminarEquip.Image = global::Projecte2CSharp.Properties.Resources.trash16;
            this.buttonEliminarEquip.Location = new System.Drawing.Point(177, 334);
            this.buttonEliminarEquip.Name = "buttonEliminarEquip";
            this.buttonEliminarEquip.Size = new System.Drawing.Size(33, 33);
            this.buttonEliminarEquip.TabIndex = 26;
            this.buttonEliminarEquip.UseVisualStyleBackColor = false;
            this.buttonEliminarEquip.Click += new System.EventHandler(this.buttonEliminarEquip_Click);
            // 
            // buttonEditTelefon
            // 
            this.buttonEditTelefon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(182)))));
            this.buttonEditTelefon.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonEditTelefon.FlatAppearance.BorderSize = 3;
            this.buttonEditTelefon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEditTelefon.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonEditTelefon.Image = global::Projecte2CSharp.Properties.Resources.edit16;
            this.buttonEditTelefon.Location = new System.Drawing.Point(549, 68);
            this.buttonEditTelefon.Name = "buttonEditTelefon";
            this.buttonEditTelefon.Size = new System.Drawing.Size(33, 33);
            this.buttonEditTelefon.TabIndex = 25;
            this.buttonEditTelefon.UseVisualStyleBackColor = false;
            this.buttonEditTelefon.Click += new System.EventHandler(this.buttonEditTelefon_Click);
            // 
            // buttonEditEquip
            // 
            this.buttonEditEquip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(182)))));
            this.buttonEditEquip.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonEditEquip.FlatAppearance.BorderSize = 3;
            this.buttonEditEquip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEditEquip.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonEditEquip.Image = global::Projecte2CSharp.Properties.Resources.edit16;
            this.buttonEditEquip.Location = new System.Drawing.Point(139, 334);
            this.buttonEditEquip.Name = "buttonEditEquip";
            this.buttonEditEquip.Size = new System.Drawing.Size(33, 33);
            this.buttonEditEquip.TabIndex = 24;
            this.buttonEditEquip.UseVisualStyleBackColor = false;
            this.buttonEditEquip.Click += new System.EventHandler(this.buttonEditEquip_Click);
            // 
            // buttonAddTelefon
            // 
            this.buttonAddTelefon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.buttonAddTelefon.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.buttonAddTelefon.FlatAppearance.BorderSize = 3;
            this.buttonAddTelefon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddTelefon.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonAddTelefon.Image = global::Projecte2CSharp.Properties.Resources.add16;
            this.buttonAddTelefon.Location = new System.Drawing.Point(511, 68);
            this.buttonAddTelefon.Name = "buttonAddTelefon";
            this.buttonAddTelefon.Size = new System.Drawing.Size(33, 33);
            this.buttonAddTelefon.TabIndex = 23;
            this.buttonAddTelefon.UseVisualStyleBackColor = false;
            this.buttonAddTelefon.Click += new System.EventHandler(this.buttonAddTelefon_Click);
            // 
            // buttonAddEquip
            // 
            this.buttonAddEquip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.buttonAddEquip.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.buttonAddEquip.FlatAppearance.BorderSize = 3;
            this.buttonAddEquip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddEquip.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonAddEquip.Image = global::Projecte2CSharp.Properties.Resources.add16;
            this.buttonAddEquip.Location = new System.Drawing.Point(100, 334);
            this.buttonAddEquip.Name = "buttonAddEquip";
            this.buttonAddEquip.Size = new System.Drawing.Size(33, 33);
            this.buttonAddEquip.TabIndex = 22;
            this.buttonAddEquip.UseVisualStyleBackColor = false;
            this.buttonAddEquip.Click += new System.EventHandler(this.buttonAddEquip_Click);
            // 
            // buttonGeneratPassword
            // 
            this.buttonGeneratPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.buttonGeneratPassword.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.buttonGeneratPassword.FlatAppearance.BorderSize = 3;
            this.buttonGeneratPassword.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGeneratPassword.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonGeneratPassword.Image = global::Projecte2CSharp.Properties.Resources.add16;
            this.buttonGeneratPassword.Location = new System.Drawing.Point(366, 267);
            this.buttonGeneratPassword.Name = "buttonGeneratPassword";
            this.buttonGeneratPassword.Size = new System.Drawing.Size(28, 28);
            this.buttonGeneratPassword.TabIndex = 21;
            this.buttonGeneratPassword.UseVisualStyleBackColor = false;
            this.buttonGeneratPassword.Click += new System.EventHandler(this.buttonGeneratPassword_Click_1);
            // 
            // textBoxBuscar
            // 
            this.textBoxBuscar.Location = new System.Drawing.Point(271, 337);
            this.textBoxBuscar.Name = "textBoxBuscar";
            this.textBoxBuscar.Size = new System.Drawing.Size(273, 28);
            this.textBoxBuscar.TabIndex = 29;
            this.textBoxBuscar.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // identitatDataGridViewTextBoxColumn
            // 
            this.identitatDataGridViewTextBoxColumn.DataPropertyName = "id_entitat";
            this.identitatDataGridViewTextBoxColumn.HeaderText = "id_entitat";
            this.identitatDataGridViewTextBoxColumn.Name = "identitatDataGridViewTextBoxColumn";
            this.identitatDataGridViewTextBoxColumn.ReadOnly = true;
            this.identitatDataGridViewTextBoxColumn.Visible = false;
            // 
            // raoDataGridViewTextBoxColumn
            // 
            this.raoDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.raoDataGridViewTextBoxColumn.DataPropertyName = "rao";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.raoDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.raoDataGridViewTextBoxColumn.HeaderText = "Raó";
            this.raoDataGridViewTextBoxColumn.Name = "raoDataGridViewTextBoxColumn";
            this.raoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // telefonDataGridViewTextBoxColumn
            // 
            this.telefonDataGridViewTextBoxColumn.DataPropertyName = "telefon";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.telefonDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.telefonDataGridViewTextBoxColumn.HeaderText = "Telèfon";
            this.telefonDataGridViewTextBoxColumn.Name = "telefonDataGridViewTextBoxColumn";
            this.telefonDataGridViewTextBoxColumn.ReadOnly = true;
            this.telefonDataGridViewTextBoxColumn.Width = 150;
            // 
            // nom
            // 
            this.nom.DataPropertyName = "nom";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.nom.DefaultCellStyle = dataGridViewCellStyle5;
            this.nom.HeaderText = "Nom";
            this.nom.Name = "nom";
            this.nom.ReadOnly = true;
            this.nom.Width = 200;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "id";
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn1.HeaderText = "id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // id_esport
            // 
            this.id_esport.DataPropertyName = "id_esport";
            this.id_esport.DataSource = this.bindingSourceEsports;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.id_esport.DefaultCellStyle = dataGridViewCellStyle7;
            this.id_esport.DisplayMember = "nom";
            this.id_esport.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.id_esport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.id_esport.HeaderText = "Esport";
            this.id_esport.Name = "id_esport";
            this.id_esport.ReadOnly = true;
            this.id_esport.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.id_esport.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.id_esport.ValueMember = "id";
            this.id_esport.Width = 150;
            // 
            // id_categoria
            // 
            this.id_categoria.DataPropertyName = "id_categoria";
            this.id_categoria.DataSource = this.bindingSourceCategories;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.id_categoria.DefaultCellStyle = dataGridViewCellStyle8;
            this.id_categoria.DisplayMember = "nom";
            this.id_categoria.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.id_categoria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.id_categoria.HeaderText = "Categoria";
            this.id_categoria.Name = "id_categoria";
            this.id_categoria.ReadOnly = true;
            this.id_categoria.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.id_categoria.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.id_categoria.ValueMember = "id";
            this.id_categoria.Width = 150;
            // 
            // id_sexe
            // 
            this.id_sexe.DataPropertyName = "id_sexe";
            this.id_sexe.DataSource = this.bindingSourceSexes;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.id_sexe.DefaultCellStyle = dataGridViewCellStyle9;
            this.id_sexe.DisplayMember = "nom";
            this.id_sexe.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.id_sexe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.id_sexe.HeaderText = "Sexe";
            this.id_sexe.Name = "id_sexe";
            this.id_sexe.ReadOnly = true;
            this.id_sexe.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.id_sexe.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.id_sexe.ValueMember = "id";
            // 
            // id_competicio
            // 
            this.id_competicio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.id_competicio.DataPropertyName = "id_competicio";
            this.id_competicio.DataSource = this.bindingSourceCompeticio;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            this.id_competicio.DefaultCellStyle = dataGridViewCellStyle10;
            this.id_competicio.DisplayMember = "nom";
            this.id_competicio.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.id_competicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.id_competicio.HeaderText = "Competició";
            this.id_competicio.Name = "id_competicio";
            this.id_competicio.ReadOnly = true;
            this.id_competicio.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.id_competicio.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.id_competicio.ValueMember = "id";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "id_entitat";
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn2.HeaderText = "id_entitat";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // EntitatForm
            // 
            this.AcceptButton = this.buttonSaveChanges;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.CancelButton = this.buttonCancelar;
            this.ClientSize = new System.Drawing.Size(772, 757);
            this.Controls.Add(this.textBoxBuscar);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonEliminarTelefon);
            this.Controls.Add(this.buttonEliminarEquip);
            this.Controls.Add(this.buttonEditTelefon);
            this.Controls.Add(this.buttonEditEquip);
            this.Controls.Add(this.buttonAddTelefon);
            this.Controls.Add(this.buttonAddEquip);
            this.Controls.Add(this.buttonGeneratPassword);
            this.Controls.Add(this.dataGridViewEquips);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dataGridViewTelefons);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxCorreu);
            this.Controls.Add(this.buttonCancelar);
            this.Controls.Add(this.buttonSaveChanges);
            this.Controls.Add(this.labelNomEntitat);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxDireccio);
            this.Controls.Add(this.textBoxCIF);
            this.Controls.Add(this.textBoxNom);
            this.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "EntitatForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Activated += new System.EventHandler(this.EntitatForm_Activated);
            this.Load += new System.EventHandler(this.EntitatForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTelefons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telefonsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEquips)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEsports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCategories)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceSexes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCompeticio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEquips)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxNom;
        private System.Windows.Forms.TextBox textBoxCIF;
        private System.Windows.Forms.TextBox textBoxDireccio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelNomEntitat;
        private System.Windows.Forms.Button buttonSaveChanges;
        private System.Windows.Forms.Button buttonCancelar;
        private System.Windows.Forms.TextBox textBoxCorreu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.DataGridView dataGridViewTelefons;
        private System.Windows.Forms.BindingSource telefonsBindingSource;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dataGridViewEquips;
        private System.Windows.Forms.Button buttonGeneratPassword;
        private System.Windows.Forms.Button buttonAddEquip;
        private System.Windows.Forms.Button buttonAddTelefon;
        private System.Windows.Forms.Button buttonEditEquip;
        private System.Windows.Forms.Button buttonEditTelefon;
        private System.Windows.Forms.Button buttonEliminarEquip;
        private System.Windows.Forms.Button buttonEliminarTelefon;
        private System.Windows.Forms.BindingSource bindingSourceEquips;
        private System.Windows.Forms.BindingSource bindingSourceEsports;
        private System.Windows.Forms.BindingSource bindingSourceSexes;
        private System.Windows.Forms.BindingSource bindingSourceCategories;
        private System.Windows.Forms.BindingSource bindingSourceCompeticio;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxBuscar;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn identitatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn raoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn telefonDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nom;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn id_esport;
        private System.Windows.Forms.DataGridViewComboBoxColumn id_categoria;
        private System.Windows.Forms.DataGridViewComboBoxColumn id_sexe;
        private System.Windows.Forms.DataGridViewComboBoxColumn id_competicio;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    }
}