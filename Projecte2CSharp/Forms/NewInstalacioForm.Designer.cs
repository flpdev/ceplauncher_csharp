﻿namespace Projecte2CSharp.Forms
{
    partial class NewInstalacioForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewInstalacioForm));
            this.labelNovaInstalacio = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxNom = new System.Windows.Forms.TextBox();
            this.textBoxDireccio = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.buttonNovaImatge = new System.Windows.Forms.Button();
            this.comboBoxHorariInici = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxHorariFi = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dataGridViewEspais = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idinstalacioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.exteriorText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.preuDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.esexteriorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.borratDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.activitatsconcedidesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activitatsdemanadesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.instalacionsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSourceEspais = new System.Windows.Forms.BindingSource(this.components);
            this.textBoxBuscar = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonCancelar = new System.Windows.Forms.Button();
            this.buttonSaveChanges = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonEliminarEquip = new System.Windows.Forms.Button();
            this.buttonEditEquip = new System.Windows.Forms.Button();
            this.buttonAddEquip = new System.Windows.Forms.Button();
            this.pictureBoxImg = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEspais)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEspais)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImg)).BeginInit();
            this.SuspendLayout();
            // 
            // labelNovaInstalacio
            // 
            this.labelNovaInstalacio.AutoSize = true;
            this.labelNovaInstalacio.Font = new System.Drawing.Font("Source Sans Pro", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNovaInstalacio.Location = new System.Drawing.Point(17, 27);
            this.labelNovaInstalacio.Name = "labelNovaInstalacio";
            this.labelNovaInstalacio.Size = new System.Drawing.Size(193, 31);
            this.labelNovaInstalacio.TabIndex = 0;
            this.labelNovaInstalacio.Text = "Nova instal·lació";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nom*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Adreça*";
            // 
            // textBoxNom
            // 
            this.textBoxNom.Location = new System.Drawing.Point(109, 86);
            this.textBoxNom.Name = "textBoxNom";
            this.textBoxNom.Size = new System.Drawing.Size(308, 28);
            this.textBoxNom.TabIndex = 3;
            this.textBoxNom.TextChanged += new System.EventHandler(this.textBoxNom_TextChanged);
            // 
            // textBoxDireccio
            // 
            this.textBoxDireccio.Location = new System.Drawing.Point(109, 131);
            this.textBoxDireccio.Name = "textBoxDireccio";
            this.textBoxDireccio.Size = new System.Drawing.Size(308, 28);
            this.textBoxDireccio.TabIndex = 4;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(24, 222);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(125, 24);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = "Gestió externa";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // buttonNovaImatge
            // 
            this.buttonNovaImatge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.buttonNovaImatge.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.buttonNovaImatge.FlatAppearance.BorderSize = 3;
            this.buttonNovaImatge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNovaImatge.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonNovaImatge.Location = new System.Drawing.Point(437, 252);
            this.buttonNovaImatge.Name = "buttonNovaImatge";
            this.buttonNovaImatge.Size = new System.Drawing.Size(181, 33);
            this.buttonNovaImatge.TabIndex = 40;
            this.buttonNovaImatge.Text = "Pujar imatge";
            this.buttonNovaImatge.UseVisualStyleBackColor = false;
            this.buttonNovaImatge.Click += new System.EventHandler(this.buttonNovaImatge_Click);
            // 
            // comboBoxHorariInici
            // 
            this.comboBoxHorariInici.FormattingEnabled = true;
            this.comboBoxHorariInici.Location = new System.Drawing.Point(138, 171);
            this.comboBoxHorariInici.Name = "comboBoxHorariInici";
            this.comboBoxHorariInici.Size = new System.Drawing.Size(121, 28);
            this.comboBoxHorariInici.TabIndex = 41;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 20);
            this.label5.TabIndex = 42;
            this.label5.Text = "Horari*";
            // 
            // comboBoxHorariFi
            // 
            this.comboBoxHorariFi.FormattingEnabled = true;
            this.comboBoxHorariFi.Location = new System.Drawing.Point(296, 171);
            this.comboBoxHorariFi.Name = "comboBoxHorariFi";
            this.comboBoxHorariFi.Size = new System.Drawing.Size(121, 28);
            this.comboBoxHorariFi.TabIndex = 44;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(269, 175);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 20);
            this.label6.TabIndex = 45;
            this.label6.Text = "a";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(105, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 20);
            this.label7.TabIndex = 46;
            this.label7.Text = "De";
            // 
            // dataGridViewEspais
            // 
            this.dataGridViewEspais.AllowUserToAddRows = false;
            this.dataGridViewEspais.AutoGenerateColumns = false;
            this.dataGridViewEspais.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewEspais.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewEspais.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridViewEspais.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewEspais.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewEspais.ColumnHeadersHeight = 35;
            this.dataGridViewEspais.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewEspais.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.idDataGridViewTextBoxColumn,
            this.idinstalacioDataGridViewTextBoxColumn,
            this.nomDataGridViewTextBoxColumn,
            this.exteriorText,
            this.preuDataGridViewTextBoxColumn,
            this.esexteriorDataGridViewTextBoxColumn,
            this.borratDataGridViewCheckBoxColumn,
            this.activitatsconcedidesDataGridViewTextBoxColumn,
            this.activitatsdemanadesDataGridViewTextBoxColumn,
            this.instalacionsDataGridViewTextBoxColumn});
            this.dataGridViewEspais.DataSource = this.bindingSourceEspais;
            this.dataGridViewEspais.EnableHeadersVisualStyles = false;
            this.dataGridViewEspais.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.dataGridViewEspais.Location = new System.Drawing.Point(24, 348);
            this.dataGridViewEspais.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dataGridViewEspais.MultiSelect = false;
            this.dataGridViewEspais.Name = "dataGridViewEspais";
            this.dataGridViewEspais.ReadOnly = true;
            this.dataGridViewEspais.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewEspais.RowHeadersVisible = false;
            this.dataGridViewEspais.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewEspais.RowTemplate.Height = 35;
            this.dataGridViewEspais.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewEspais.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewEspais.Size = new System.Drawing.Size(594, 203);
            this.dataGridViewEspais.TabIndex = 47;
            this.dataGridViewEspais.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridViewEspais_UserDeletingRow);
            this.dataGridViewEspais.DoubleClick += new System.EventHandler(this.dataGridViewEspais_DoubleClick);
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.DataPropertyName = "id";
            this.Column1.HeaderText = "id";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // idinstalacioDataGridViewTextBoxColumn
            // 
            this.idinstalacioDataGridViewTextBoxColumn.DataPropertyName = "id_instalacio";
            this.idinstalacioDataGridViewTextBoxColumn.HeaderText = "id_instalacio";
            this.idinstalacioDataGridViewTextBoxColumn.Name = "idinstalacioDataGridViewTextBoxColumn";
            this.idinstalacioDataGridViewTextBoxColumn.ReadOnly = true;
            this.idinstalacioDataGridViewTextBoxColumn.Visible = false;
            // 
            // nomDataGridViewTextBoxColumn
            // 
            this.nomDataGridViewTextBoxColumn.DataPropertyName = "nom";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.nomDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.nomDataGridViewTextBoxColumn.HeaderText = "Nom";
            this.nomDataGridViewTextBoxColumn.Name = "nomDataGridViewTextBoxColumn";
            this.nomDataGridViewTextBoxColumn.ReadOnly = true;
            this.nomDataGridViewTextBoxColumn.Width = 300;
            // 
            // exteriorText
            // 
            this.exteriorText.DataPropertyName = "exteriorText";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.exteriorText.DefaultCellStyle = dataGridViewCellStyle3;
            this.exteriorText.HeaderText = "Exterior";
            this.exteriorText.Name = "exteriorText";
            this.exteriorText.ReadOnly = true;
            // 
            // preuDataGridViewTextBoxColumn
            // 
            this.preuDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.preuDataGridViewTextBoxColumn.DataPropertyName = "preu";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.preuDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.preuDataGridViewTextBoxColumn.HeaderText = "Preu";
            this.preuDataGridViewTextBoxColumn.Name = "preuDataGridViewTextBoxColumn";
            this.preuDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // esexteriorDataGridViewTextBoxColumn
            // 
            this.esexteriorDataGridViewTextBoxColumn.DataPropertyName = "es_exterior";
            this.esexteriorDataGridViewTextBoxColumn.HeaderText = "es_exterior";
            this.esexteriorDataGridViewTextBoxColumn.Name = "esexteriorDataGridViewTextBoxColumn";
            this.esexteriorDataGridViewTextBoxColumn.ReadOnly = true;
            this.esexteriorDataGridViewTextBoxColumn.Visible = false;
            // 
            // borratDataGridViewCheckBoxColumn
            // 
            this.borratDataGridViewCheckBoxColumn.DataPropertyName = "borrat";
            this.borratDataGridViewCheckBoxColumn.HeaderText = "borrat";
            this.borratDataGridViewCheckBoxColumn.Name = "borratDataGridViewCheckBoxColumn";
            this.borratDataGridViewCheckBoxColumn.ReadOnly = true;
            this.borratDataGridViewCheckBoxColumn.Visible = false;
            // 
            // activitatsconcedidesDataGridViewTextBoxColumn
            // 
            this.activitatsconcedidesDataGridViewTextBoxColumn.DataPropertyName = "activitats_concedides";
            this.activitatsconcedidesDataGridViewTextBoxColumn.HeaderText = "activitats_concedides";
            this.activitatsconcedidesDataGridViewTextBoxColumn.Name = "activitatsconcedidesDataGridViewTextBoxColumn";
            this.activitatsconcedidesDataGridViewTextBoxColumn.ReadOnly = true;
            this.activitatsconcedidesDataGridViewTextBoxColumn.Visible = false;
            // 
            // activitatsdemanadesDataGridViewTextBoxColumn
            // 
            this.activitatsdemanadesDataGridViewTextBoxColumn.DataPropertyName = "activitats_demanades";
            this.activitatsdemanadesDataGridViewTextBoxColumn.HeaderText = "activitats_demanades";
            this.activitatsdemanadesDataGridViewTextBoxColumn.Name = "activitatsdemanadesDataGridViewTextBoxColumn";
            this.activitatsdemanadesDataGridViewTextBoxColumn.ReadOnly = true;
            this.activitatsdemanadesDataGridViewTextBoxColumn.Visible = false;
            // 
            // instalacionsDataGridViewTextBoxColumn
            // 
            this.instalacionsDataGridViewTextBoxColumn.DataPropertyName = "instalacions";
            this.instalacionsDataGridViewTextBoxColumn.HeaderText = "instalacions";
            this.instalacionsDataGridViewTextBoxColumn.Name = "instalacionsDataGridViewTextBoxColumn";
            this.instalacionsDataGridViewTextBoxColumn.ReadOnly = true;
            this.instalacionsDataGridViewTextBoxColumn.Visible = false;
            // 
            // bindingSourceEspais
            // 
            this.bindingSourceEspais.DataSource = typeof(Projecte2CSharp.espais);
            // 
            // textBoxBuscar
            // 
            this.textBoxBuscar.Location = new System.Drawing.Point(310, 312);
            this.textBoxBuscar.Name = "textBoxBuscar";
            this.textBoxBuscar.Size = new System.Drawing.Size(308, 28);
            this.textBoxBuscar.TabIndex = 53;
            this.textBoxBuscar.TextChanged += new System.EventHandler(this.textBoxBuscar_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 313);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 20);
            this.label1.TabIndex = 48;
            this.label1.Text = "Espais";
            // 
            // buttonCancelar
            // 
            this.buttonCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(181)))));
            this.buttonCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancelar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonCancelar.FlatAppearance.BorderSize = 3;
            this.buttonCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancelar.Location = new System.Drawing.Point(286, 577);
            this.buttonCancelar.Name = "buttonCancelar";
            this.buttonCancelar.Size = new System.Drawing.Size(119, 51);
            this.buttonCancelar.TabIndex = 55;
            this.buttonCancelar.Text = "Cancel·lar";
            this.buttonCancelar.UseVisualStyleBackColor = false;
            this.buttonCancelar.Click += new System.EventHandler(this.buttonCancelar_Click);
            // 
            // buttonSaveChanges
            // 
            this.buttonSaveChanges.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.buttonSaveChanges.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.buttonSaveChanges.FlatAppearance.BorderSize = 3;
            this.buttonSaveChanges.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSaveChanges.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonSaveChanges.Location = new System.Drawing.Point(432, 577);
            this.buttonSaveChanges.Name = "buttonSaveChanges";
            this.buttonSaveChanges.Size = new System.Drawing.Size(186, 51);
            this.buttonSaveChanges.TabIndex = 54;
            this.buttonSaveChanges.Text = "Guardar canvis";
            this.buttonSaveChanges.UseVisualStyleBackColor = false;
            this.buttonSaveChanges.Click += new System.EventHandler(this.buttonSaveChanges_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = global::Projecte2CSharp.Properties.Resources.search;
            this.button1.Location = new System.Drawing.Point(267, 309);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(37, 33);
            this.button1.TabIndex = 52;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // buttonEliminarEquip
            // 
            this.buttonEliminarEquip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(181)))));
            this.buttonEliminarEquip.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonEliminarEquip.FlatAppearance.BorderSize = 3;
            this.buttonEliminarEquip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEliminarEquip.Image = global::Projecte2CSharp.Properties.Resources.trash16;
            this.buttonEliminarEquip.Location = new System.Drawing.Point(204, 309);
            this.buttonEliminarEquip.Name = "buttonEliminarEquip";
            this.buttonEliminarEquip.Size = new System.Drawing.Size(37, 33);
            this.buttonEliminarEquip.TabIndex = 51;
            this.buttonEliminarEquip.UseVisualStyleBackColor = false;
            this.buttonEliminarEquip.Click += new System.EventHandler(this.buttonEliminarEquip_Click);
            // 
            // buttonEditEquip
            // 
            this.buttonEditEquip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(182)))));
            this.buttonEditEquip.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonEditEquip.FlatAppearance.BorderSize = 3;
            this.buttonEditEquip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEditEquip.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonEditEquip.Image = global::Projecte2CSharp.Properties.Resources.edit16;
            this.buttonEditEquip.Location = new System.Drawing.Point(161, 309);
            this.buttonEditEquip.Name = "buttonEditEquip";
            this.buttonEditEquip.Size = new System.Drawing.Size(37, 33);
            this.buttonEditEquip.TabIndex = 50;
            this.buttonEditEquip.UseVisualStyleBackColor = false;
            this.buttonEditEquip.Click += new System.EventHandler(this.buttonEditEquip_Click);
            // 
            // buttonAddEquip
            // 
            this.buttonAddEquip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.buttonAddEquip.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.buttonAddEquip.FlatAppearance.BorderSize = 3;
            this.buttonAddEquip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddEquip.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonAddEquip.Image = global::Projecte2CSharp.Properties.Resources.add16;
            this.buttonAddEquip.Location = new System.Drawing.Point(118, 309);
            this.buttonAddEquip.Name = "buttonAddEquip";
            this.buttonAddEquip.Size = new System.Drawing.Size(37, 33);
            this.buttonAddEquip.TabIndex = 49;
            this.buttonAddEquip.UseVisualStyleBackColor = false;
            this.buttonAddEquip.Click += new System.EventHandler(this.buttonAddEquip_Click);
            // 
            // pictureBoxImg
            // 
            this.pictureBoxImg.ErrorImage = global::Projecte2CSharp.Properties.Resources.noimage;
            this.pictureBoxImg.Image = global::Projecte2CSharp.Properties.Resources.noimage;
            this.pictureBoxImg.Location = new System.Drawing.Point(437, 86);
            this.pictureBoxImg.Name = "pictureBoxImg";
            this.pictureBoxImg.Size = new System.Drawing.Size(181, 160);
            this.pictureBoxImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxImg.TabIndex = 10;
            this.pictureBoxImg.TabStop = false;
            // 
            // NewInstalacioForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(637, 648);
            this.Controls.Add(this.buttonCancelar);
            this.Controls.Add(this.buttonSaveChanges);
            this.Controls.Add(this.textBoxBuscar);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonEliminarEquip);
            this.Controls.Add(this.buttonEditEquip);
            this.Controls.Add(this.buttonAddEquip);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridViewEspais);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.comboBoxHorariFi);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBoxHorariInici);
            this.Controls.Add(this.buttonNovaImatge);
            this.Controls.Add(this.pictureBoxImg);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.textBoxDireccio);
            this.Controls.Add(this.textBoxNom);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelNovaInstalacio);
            this.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "NewInstalacioForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Nova Instal·lació";
            this.Activated += new System.EventHandler(this.NewInstalacioForm_Activated);
            this.Load += new System.EventHandler(this.NewInstalacioForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEspais)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEspais)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImg)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNovaInstalacio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxNom;
        private System.Windows.Forms.TextBox textBoxDireccio;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.PictureBox pictureBoxImg;
        private System.Windows.Forms.Button buttonNovaImatge;
        private System.Windows.Forms.ComboBox comboBoxHorariInici;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxHorariFi;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dataGridViewEspais;
        private System.Windows.Forms.BindingSource bindingSourceEspais;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idinstalacioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn exteriorText;
        private System.Windows.Forms.DataGridViewTextBoxColumn preuDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn preuespecialDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn esexteriorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn borratDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn activitatsconcedidesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn activitatsdemanadesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn instalacionsDataGridViewTextBoxColumn;
        private System.Windows.Forms.TextBox textBoxBuscar;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonEliminarEquip;
        private System.Windows.Forms.Button buttonEditEquip;
        private System.Windows.Forms.Button buttonAddEquip;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonCancelar;
        private System.Windows.Forms.Button buttonSaveChanges;
    }
}