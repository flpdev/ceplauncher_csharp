﻿using Projecte2CSharp.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.Forms
{
    public partial class NewInstalacioForm : Form
    {
        public List<string> horaris = new List<string>();
        public instalacions instalacio { get; set; }
        public bool isModification { get; set; } = false;
        public string IMG_PATH = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\..\\recursos\\imagenes\\"));
        public string imatge { get; set; }
        public NewInstalacioForm()
        {
            InitializeComponent();
        }

        public NewInstalacioForm(instalacions instalacio)
        {
            this.instalacio = instalacio;
            this.isModification = true;
            InitializeComponent();
        }
        
    private void NewInstalacioForm_Load(object sender, EventArgs e)
        {
            //MessageBox.Show();
            //Emplenar bindingSources
            comboBoxHorariInici.DataSource = NewInstalacioForm.GenerarHoraris();
            comboBoxHorariFi.DataSource = NewInstalacioForm.GenerarHoraris();
            horaris = NewInstalacioForm.GenerarHoraris();

            //modificacio
            if (isModification)
            {
                if (instalacio.imatge != null)
                {
                    string img = IMG_PATH + instalacio.imatge + ".jpeg";
                    pictureBoxImg.Image = new Bitmap(img);
                    imatge = instalacio.imatge;
                }

                //canviar text form
                this.Text = instalacio.nom;


                //carregar data source
                string msg = "";
                bindingSourceEspais.DataSource = ORMEspais.selectEspaisByInstalacio(instalacio.id, ref msg);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                //carregar camps 
                labelNovaInstalacio.Text = instalacio.nom;
                textBoxNom.Text = instalacio.nom;
                textBoxDireccio.Text = instalacio.direccio;
                checkBox1.Checked = instalacio.gestio_externa ? true : false;

                //Carrega del horari de la instalacio al comboBox
                for(int i=0; i<horaris.Count(); i++)
                {
                    TimeSpan tsInici = new TimeSpan();
                    tsInici = (TimeSpan)instalacio.hora_inici;
                    string sInici = new DateTime(tsInici.Ticks).ToString("HH':'mm");

                    TimeSpan tsFinal = new TimeSpan();
                    tsFinal = (TimeSpan)instalacio.hora_final;
                    string sFinal = new DateTime(tsFinal.Ticks).ToString("HH':'mm");

                    if (horaris.ElementAt(i).Equals(sInici))
                    {
                        comboBoxHorariInici.SelectedIndex = i;
                    }
                    if (horaris.ElementAt(i).Equals(sFinal))
                    {
                        comboBoxHorariFi.SelectedIndex = i;
                    }
                }
                
            }


            //Nova instalacio
            else
            {
                instalacio = new instalacions();
                this.Text = "Nova instal·lació";
                labelNovaInstalacio.Text = "Nova instal·lació";
            }
            
        }

        public static List<string> GenerarHoraris()
        {
            List<string> _horaris = new List<string>();

            for(int i=1; i<24; i++)
            {
                TimeSpan ts = new TimeSpan(i, 0, 0);
                string s = new DateTime(ts.Ticks).ToString("HH':'mm");
                _horaris.Add(s);
                if (i == 23)
                {
                    ts = new TimeSpan(0, 0, 0);
                    s = new DateTime(ts.Ticks).ToString("HH':'mm");
                    _horaris.Add(s);
                }
            }
            

            return _horaris;
        }

        public string generateFileName()
        {
            string fileName;
            DateTime today = DateTime.Now;
            fileName = today.ToString("ddMMyyyyhhss") + "_" + textBoxNom.Text.Replace(" ", "_");
            return fileName;
        }

        private void buttonSaveChanges_Click(object sender, EventArgs e)
        {
            if (!isModification) { instalacio = new instalacions(); }

            instalacio.nom = textBoxNom.Text;
            instalacio.direccio = textBoxDireccio.Text;
            instalacio.gestio_externa = checkBox1.Checked;
            instalacio.imatge = imatge;

            instalacio.hora_inici = new TimeSpan(Int32.Parse(comboBoxHorariInici.SelectedValue.ToString().Split(':')[0]), Int32.Parse(comboBoxHorariInici.SelectedValue.ToString().Split(':')[1]), 0);
            instalacio.hora_final = new TimeSpan(Int32.Parse(comboBoxHorariFi.SelectedValue.ToString().Split(':')[0]), Int32.Parse(comboBoxHorariFi.SelectedValue.ToString().Split(':')[1]), 0);

            if (isModification)
            {
                string msg = ORMInstalacions.updateInstalacio(instalacio);

                if (!msg.Equals(""))
                {
                    MessageBox.Show("Algo ha anat malament quan es guardaven els canvis", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
               
            }
            else
            {
                string msg = ORMInstalacions.insertInstalacio(instalacio);

                if (!msg.Equals(""))
                {
                    MessageBox.Show("Algo ha anat malament quan es guardaven els canvis", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            this.Close();
            
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBoxNom_TextChanged(object sender, EventArgs e)
        {
            labelNovaInstalacio.Text = textBoxNom.Text;
        }

        private void buttonNovaImatge_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();

            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";

            if (open.ShowDialog() == DialogResult.OK)
            {
                Bitmap img = new Bitmap(open.FileName);
                string filename = generateFileName();
                pictureBoxImg.Image = img;
                //string path = "C:\\Users\\Ferran\\Desktop\\img\\"+ "hoooola" + ".jpeg";
                string path = IMG_PATH + filename + ".jpeg";

                //img.Save(@"C:\\Users\\Ferran\Desktop\\img");
                //pictureBoxImg.Image.Save(path, ImageFormat.Jpeg);

                pictureBoxImg.Image.Save(path);
                //File.Delete(path);
                //img.Save(@Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\..\\recursos\\imagenes")));
                imatge = filename;
            }
        }

        private void dataGridViewEspais_DoubleClick(object sender, EventArgs e)
        {
            NewEspaiForm nef = new NewEspaiForm((espais)dataGridViewEspais.CurrentRow.DataBoundItem, instalacio.id);
            nef.ShowDialog();
        }

        private void buttonAddEquip_Click(object sender, EventArgs e)
        {
            NewEspaiForm nef = new NewEspaiForm(instalacio);
            nef.ShowDialog();
        }

        private void buttonEditEquip_Click(object sender, EventArgs e)
        {
            NewEspaiForm nef = new NewEspaiForm((espais)dataGridViewEspais.CurrentRow.DataBoundItem, instalacio.id);
            nef.ShowDialog();
        }

        private void buttonEliminarEquip_Click(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("Segur que vols eliminar " + ((espais)dataGridViewEspais.CurrentRow.DataBoundItem).nom + "?", "Eliminar espai", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (r == DialogResult.Yes)
            {
                string msg = "";
                ORMEspais.deleteEspai((espais)dataGridViewEspais.CurrentRow.DataBoundItem);
                bindingSourceEspais.DataSource = ORMEspais.selectEspaisByInstalacio(instalacio.id, ref msg);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void dataGridViewEspais_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            DialogResult r = MessageBox.Show("Segur que vols eliminar " + ((espais)dataGridViewEspais.CurrentRow.DataBoundItem).nom + "?", "Eliminar espai", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (r == DialogResult.Yes)
            {
                string msg = "";
                ORMEspais.deleteEspai((espais)dataGridViewEspais.CurrentRow.DataBoundItem);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
               
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void NewInstalacioForm_Activated(object sender, EventArgs e)
        {
            string msg = "";
            bindingSourceEspais.DataSource = ORMEspais.selectEspaisByInstalacio(instalacio.id, ref msg);
            if (!msg.Equals(""))
            {

                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBoxBuscar_TextChanged(object sender, EventArgs e)
        {
            if (isModification)
            {
                string msg = "";
                bindingSourceEspais.DataSource = ORMEspais.selectEspaisByNomIInstalacio(instalacio.id, textBoxBuscar.Text, ref msg);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            
        }
    }
}
