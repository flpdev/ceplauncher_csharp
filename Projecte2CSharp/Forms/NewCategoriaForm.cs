﻿using Projecte2CSharp.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.Forms
{
    public partial class NewCategoriaForm : Form
    {
        public categories categoria { get; set; }
        public bool isModification { get; set; }
        public NewCategoriaForm()
        {
            InitializeComponent();
        }

        public NewCategoriaForm(categories categoria)
        {
            isModification = true;
            this.categoria = categoria;
            InitializeComponent();
        }

        private void NewCategoriaForm_Load(object sender, EventArgs e)
        {
            if (isModification)
            {
                textBoxNom.Text = categoria.nom;
                this.Text = categoria.nom;

            }
        }

        private void buttonSaveChanges_Click(object sender, EventArgs e)
        {
            if (!isModification) { categoria = new categories(); }

            categoria.nom = textBoxNom.Text;
            string msg = "";
            if (isModification)
            {
                msg = ORMCategories.updateCategoria(categoria);
            }
            else
            {
                msg = ORMCategories.insertCategoria(categoria);
            }

            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.Close();
        }
    }
}
