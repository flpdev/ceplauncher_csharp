﻿using Projecte2CSharp.Clases.POJO;
using Projecte2CSharp.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Projecte2CSharp.Forms
{
    public partial class FormDemanda : Form
    {
        public ActivitatDemanada activitat { get; set; }
        public bool activitatCreada { get; set; }
        public int id_activitatConcedida { get; set; }
        public int id_user { get; set; }
        public FormDemanda()
        {
            InitializeComponent();
        }

        public FormDemanda(ActivitatDemanada activitat, int id_user)
        {
            this.id_user = id_user;
            this.activitat = activitat;
            InitializeComponent();
        }

        private void FormDemanda_Load(object sender, EventArgs e)
        {
            refreshData();
            checkActivitat();
            
            //listBoxHoraris.DataSource = this.generateHoraris();
            if (comboBoxHorariInici.Items.Count > 0 && comboBoxHorariFi.Items.Count > 0)
            {
                if (comboBoxHorariInici.SelectedIndex + activitat.durada < comboBoxHorariInici.Items.Count) ;
                comboBoxHorariFi.SelectedIndex = comboBoxHorariInici.SelectedIndex + activitat.durada;
            }
        }



        public List<string> generateHoraris()
        {
            List<string> horaris = new List<string>();
            string msg = "";
            int num = ((instalacions)comboBoxInstalacions.SelectedItem).id;
            instalacions instalacio = ORMInstalacions.getInsatalacioByID(num, ref msg);

            
            TimeSpan tsInici = (TimeSpan)instalacio.hora_inici;
            string sInici = new DateTime(tsInici.Ticks).ToString("HH':'mm");

            TimeSpan tsFinal = (TimeSpan)instalacio.hora_final;
            string sFinal = new DateTime(tsFinal.Ticks).ToString("HH':'mm");

            int inici = int.Parse(sInici.Split(':')[0]);
            int final = int.Parse(sFinal.Split(':')[0]);

            for (int i = 0; i < final; i+=2)
            {
                string h = "de " + inici + ":00 a " + (inici +1)  + ":00";
                inici ++;
                horaris.Add(h);
            }

             inici = int.Parse(sInici.Split(':')[0]);
             final = int.Parse(sFinal.Split(':')[0]);

            //MIRAR HORARIS DISPONIBLES
            List<activitats_concedides> act = ORMActivitatConcedida.selectActivitatConcedidaByInstalacio((int)comboBoxInstalacions.SelectedValue, ref msg);
            
            foreach(activitats_concedides ac in act.ToList())
            {
                foreach(horaris_activitats h in ac.horaris_activitats.ToList())
                {
                    int id = h.id_dia_setmana;

                    horaris_adients dia = (horaris_adients)dataGridViewHoraris.CurrentRow.DataBoundItem;

                    if (id == dia.id_dia_setmana)
                    {
                        for (int i = inici; i < final; i++)
                        {
                            TimeSpan tsInici2 = (TimeSpan)h.hora_inici;
                            string sInici2 = new DateTime(tsInici2.Ticks).ToString("HH':'mm");
                            int inici2 = int.Parse(sInici2.Split(':')[0]);

                            TimeSpan tsFinal2 = (TimeSpan)h.hora_final;
                            string sFinal2 = new DateTime(tsFinal2.Ticks).ToString("HH':'mm");
                            int final2 = int.Parse(sFinal2.Split(':')[0]);

                            if (inici2 == i)
                            {
                                for(int j = 0 ; j<activitat.durada; j++)
                                {
                                    horaris[inici2-inici+j] = "No disponible";
                                }

                            }
                        }
                    }
                }
            }

            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            return horaris;

        }

        private void comboBoxInstalacions_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBoxHoraris.DataSource = generateHoraris();
            
            comboBoxHorariInici.DataSource =  getHoraris();
            comboBoxHorariFi.DataSource = getHoraris();

            if (comboBoxHorariInici.Items.Count > 0 && comboBoxHorariFi.Items.Count > 0)
            {
                if (comboBoxHorariInici.SelectedIndex + activitat.durada < comboBoxHorariInici.Items.Count) ;
                comboBoxHorariFi.SelectedIndex = comboBoxHorariInici.SelectedIndex + activitat.durada;
            }
        }

        private void dataGridViewHoraris_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            listBoxHoraris.DataSource = generateHoraris();
        }


        private void dataGridViewHoraris_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            listBoxHoraris.DataSource = generateHoraris();
        }

        private void dataGridViewHoraris_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            listBoxHoraris.DataSource = generateHoraris();
        }

        public void refreshData()
        {
            //generateHoraris();
            string msg = "";
            bindingSourceHoraris.DataSource = ORMHorarisAdients.selectHorarisAdients(activitat.id, ref msg);
            bindingSourceDies.DataSource = ORMDies.getDies(ref msg);
            bindingSourceInstalacions.DataSource = ORMInstalacions.selectInstalacions(ref msg);
            bindingSourceEspais.DataSource = ORMEspais.selectEspaisByInstalacio(activitat.id_instalacio, ref msg);
            equips equip = ORMEquips.selectEquipByID(activitat.id_equip, ref msg);
            entitats entitat = ORMEntitats.selectEntitatByID(equip.id_entitat, ref msg);

            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            labelEntitat.Text = entitat.nom + ": " + equip.nom;
            labelNumHores.Text = activitat.durada.ToString();
            comboBoxInstalacions.SelectedValue = activitat.id_instalacio;
            comboBoxEspais.SelectedValue = activitat.id_espai;
            listBoxHoraris.DataSource = generateHoraris();
            comboBoxHorariInici.DataSource = getHoraris();
            comboBoxHorariFi.DataSource = getHoraris();
            labelTipus.Text = activitat.tipus;
            
        }

        private void comboBoxHorariInici_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBoxHorariInici.Items.Count > 0 && comboBoxHorariFi.Items.Count > 0)
            {
                if (comboBoxHorariInici.SelectedIndex + activitat.durada < comboBoxHorariInici.Items.Count)
                {
                    //comboBoxHorariFi.SelectedIndex = comboBoxHorariInici.SelectedIndex + activitat.durada;
                }
                    
            }
           
        }


        public List<string> getHoraris()
        {

            List<string> horari = new List<string>();
            string msg = "";
            try
            {
                int num = (int)comboBoxInstalacions.SelectedValue;
                instalacions instalacio = ORMInstalacions.getInsatalacioByID(num, ref msg);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                TimeSpan tsInici = (TimeSpan)instalacio.hora_inici;
                string sInici = new DateTime(tsInici.Ticks).ToString("HH':'mm");
                int inici = int.Parse(sInici.Split(':')[0]);

                TimeSpan tsFinal = (TimeSpan)instalacio.hora_final;
                string sFinal = new DateTime(tsFinal.Ticks).ToString("HH':'mm");
                int final = int.Parse(sFinal.Split(':')[0]);

                for (int i = inici; i < final + 1; i++)
                {
                    horari.Add(i.ToString() + ":00");
                }
            }catch(System.NullReferenceException e)
            {
                this.Close();
            }
            

            return horari;
        }

        private void comboBoxHorariFi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxHorariInici.Items.Count > 0 && comboBoxHorariFi.Items.Count > 0)
            {
                if(comboBoxHorariFi.SelectedIndex - activitat.durada > 0)
                comboBoxHorariInici.SelectedIndex = comboBoxHorariFi.SelectedIndex - activitat.durada;
            }
        }

        private void buttonAssignar_Click(object sender, EventArgs e)
        {
            string msg = "";
            //guardar activitat
            if (!activitatCreada)
            {
                activitats_concedides actConcedida = new activitats_concedides();
                actConcedida.id_activitat_demanada = activitat.id;
                actConcedida.id_equip = activitat.id_equip;
                actConcedida.id_espai = activitat.id_espai;
                actConcedida.nom = activitat.nom;
                actConcedida.tipus = activitat.tipus;
                actConcedida.id_usuari = id_user;
                
                horaris_activitats h = new horaris_activitats();
                h.id_dia_setmana = ((horaris_adients)dataGridViewHoraris.CurrentRow.DataBoundItem).id_dia_setmana;
                TimeSpan tsInici = new TimeSpan(int.Parse(comboBoxHorariInici.SelectedValue.ToString().Split(':')[0]), 0, 0);
                TimeSpan tsFinal = new TimeSpan(int.Parse(comboBoxHorariFi.SelectedValue.ToString().Split(':')[0]), 0, 0);
                h.hora_inici = tsInici;
                h.hora_final = tsFinal;

                //update horari datagridView
                horaris_adients hadient = (horaris_adients)dataGridViewHoraris.CurrentRow.DataBoundItem;
                hadient.assignat = true;
                msg = ORMHorarisAdients.updateHorariAdient(hadient);

                actConcedida.horaris_activitats.Add(h);

                //MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                msg = ORMActivitatConcedida.insertActivitatConcedida(actConcedida);
                //MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                id_activitatConcedida = ORMActivitatConcedida.getLastInsert(ref msg).id;
                activitatCreada = true;
            }
            else
            {
                //ja exixteix la activitat concedida
                horaris_activitats h = new horaris_activitats();
                h.id_dia_setmana = ((horaris_adients)dataGridViewHoraris.CurrentRow.DataBoundItem).id_dia_setmana;
                TimeSpan tsInici = new TimeSpan(int.Parse(comboBoxHorariInici.SelectedValue.ToString().Split(':')[0]), 0, 0);
                TimeSpan tsFinal = new TimeSpan(int.Parse(comboBoxHorariFi.SelectedValue.ToString().Split(':')[0]), 0, 0);
                h.hora_inici = tsInici;
                h.hora_final = tsFinal;

                horaris_adients hadient = (horaris_adients)dataGridViewHoraris.CurrentRow.DataBoundItem;
                hadient.assignat = true;

                msg = ORMHorarisAdients.updateHorariAdient(hadient);
                activitats_concedides a = ORMActivitatConcedida.selectByID(id_activitatConcedida, ref msg);
                a.horaris_activitats.Add(h);
                msg = ORMActivitatConcedida.update(a);
            }

            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            refreshData();


        }

        public void checkActivitat()
        {
            //SI ALGUN HORARI ESTA ASSIGNAT ES QUE EXISTEIX LA ACTIVITAT CONCEDIDA
            string msg = "";
            List<horaris_adients> h = ORMHorarisAdients.selectHorarisAdients(activitat.id, ref msg);
            bool existeix = false;

            foreach(horaris_adients ha in h.ToList())
            {
                if ((bool)ha.assignat)
                {
                    existeix = true;
                }
            }

            if (existeix)
            {
                id_activitatConcedida = ((activitats_concedides)ORMActivitatConcedida.getActivitatByDemanda(activitat.id, ref msg)).id;
                //MessageBox.Show("existeix", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                activitatCreada = true;
            }

            
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            int num = (int)comboBoxInstalacions.SelectedValue;
            this.Close();
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            int num = int.Parse(comboBoxInstalacions.SelectedValue.ToString());
            Close();
        }

        private void buttonDeleteHorari_Click(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("Segur que vols desfer la assignació?", "Desfer assignació", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                string msg = "";
                horaris_adients hadient = (horaris_adients)dataGridViewHoraris.CurrentRow.DataBoundItem;
                hadient.assignat = false;
                msg = ORMHorarisAdients.updateHorariAdient(hadient);
                activitats_concedides a =  ORMActivitatConcedida.selectByID(id_activitatConcedida, ref msg);
                horaris_activitats h = ORMHoraris_activitats.selectByHorariAdient(hadient, ref msg, a.id_activitat_demanada);
                msg = ORMHoraris_activitats.deleteHorariActivitat(h);

                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                refreshData();
            }
            

        }
    }

    
}
