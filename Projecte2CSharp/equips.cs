//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Projecte2CSharp
{
    using System;
    using System.Collections.Generic;
    
    public partial class equips
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public equips()
        {
            this.activitats_concedides = new HashSet<activitats_concedides>();
            this.activitats_demanades = new HashSet<activitats_demanades>();
        }
    
        public int id { get; set; }
        public string nom { get; set; }
        public int id_entitat { get; set; }
        public int id_categoria { get; set; }
        public int id_esport { get; set; }
        public int id_competicio { get; set; }
        public Nullable<int> id_categoria_competicio { get; set; }
        public int id_sexe { get; set; }
        public bool borrat { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<activitats_concedides> activitats_concedides { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<activitats_demanades> activitats_demanades { get; set; }
        public virtual categories categories { get; set; }
        public virtual categories_competicio categories_competicio { get; set; }
        public virtual competicions competicions { get; set; }
        public virtual entitats entitats { get; set; }
        public virtual esports esports { get; set; }
        public virtual sexes sexes { get; set; }
    }
}
