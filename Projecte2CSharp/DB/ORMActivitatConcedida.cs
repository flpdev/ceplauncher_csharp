﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.DB
{
    public static class ORMActivitatConcedida
    {

        //SELECT ACTIVITATS CONCEDIDES PER EL ID DEL EQUIP
        public static List<activitats_concedides> selectActivitatConcedidaByEquip(int id, ref string exMessage)
        {
            List<activitats_concedides> act = null;
            try
            {
                act = (
                   from ac in ORM.db.activitats_concedides
                   where ac.id_equip == id
                   select ac).ToList();

               
            }catch(SqlException e)
            {
                exMessage = ORM.MensajeError(e);
            }

            return act;
        }

        public static List<activitats_concedides> selectActivitatConcedidaByInstalacio(int id, ref string msg)
        {
            List<activitats_concedides> act = null;
            try
            {
                act = (
                   from ac in ORM.db.activitats_concedides
                   where ac.espais.instalacions.id == id 
                   select ac).ToList();

                
            }
            catch (SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return act;
        }

        //INSERT ACTIVITAT CONCEDIDA

        public static string insertActivitatConcedida(activitats_concedides activitat)
        {
            ORM.db.activitats_concedides.Add(activitat);
            return ORM.saveChanges();

        }

        public static activitats_concedides getLastInsert(ref string msg)
        {
            activitats_concedides act = null;
            List<activitats_concedides> _list = null;
            try
            {
                _list = (from a in ORM.db.activitats_concedides
                       orderby a.id descending
                       select a).ToList();

                act = _list[0];
            }catch(SqlException e)
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                msg = ORM.MensajeError(e);
            }

            return act;
        }

        //Select activitat concedida by id de la activitat demanada
        public static activitats_concedides getActivitatByDemanda(int id, ref string msg)
        {
            activitats_concedides act = null;

            try
            {
                act = (from a in ORM.db.activitats_concedides
                       where a.id_activitat_demanada == id
                       select a).FirstOrDefault();

            }catch(SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return act;
        }

        public static string update(activitats_concedides act)
        {
            activitats_concedides a = ORM.db.activitats_concedides.Find(act.id);
            a = act;

            return ORM.saveChanges();
        }


        //SELECT BY ID
        public static activitats_concedides selectByID(int id, ref string msg)
        {
            activitats_concedides a = null;
            try
            {
               a = ORM.db.activitats_concedides.Find(id);
            }catch(SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return a;

        }
    }
}
