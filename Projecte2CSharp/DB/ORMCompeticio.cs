﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projecte2CSharp.DB
{
    public static class ORMCompeticio
    {

        //SELECT DE TOTES LES COMPETICIONS
        public static List<competicions> selectCompeticions(ref string msg)
        {
            List<competicions> _competicions = null;


            try
            {
                _competicions = (
                        from c in ORM.db.competicions
                        select c).ToList();
            }
            catch (SqlException e)
            {

                msg = ORM.MensajeError(e);
            }

            return _competicions;
            
        }
    }
}
