﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projecte2CSharp.DB
{
    public static class ORMSexes
    {

        //SELECT SEXES
        public static List<sexes> selectSexes(ref string msg)
        {
            List<sexes> _sexes = null;

            try
            {
                _sexes = (
                        from s in ORM.db.sexes
                        select s).ToList();
            }
            catch (SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return _sexes;
        }
    }
}
