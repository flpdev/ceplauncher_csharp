﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projecte2CSharp.DB
{
    public static class ORMTelefons
    {
        public static List<telefons> getTelefonsByEntitat(int id)
        {
            List<telefons> _telefons = (
                from t in ORM.db.telefons
                where t.id_entitat == id && t.borrat == false
                orderby t.rao
                select t).ToList();

            return _telefons;
        }

        public static void updateTelefon(telefons telefon)
        {
            telefons tel = ORM.db.telefons.Find(telefon.id);
            tel = telefon;
            ORM.db.SaveChanges();
        }

        public static void insertTelefon(telefons telefon)
        {
            ORM.db.telefons.Add(telefon);
            ORM.db.SaveChanges();
        }

        public static void deleteTelefon(telefons telefon)
        {
            telefon.borrat = true;
            telefons tel = ORM.db.telefons.Find(telefon.id);
            tel = telefon;
            ORM.db.SaveChanges();
        }
    }
}
