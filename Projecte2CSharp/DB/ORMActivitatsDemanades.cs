﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projecte2CSharp.DB
{
    public static class ORMActivitatsConcedides
    {
        public static List<activitats_demanades> getActivitatsDemanades(ref string msg)
        {
            List<activitats_demanades> _activitats = null;

            try
            {
                _activitats = (
                    from a in ORM.db.activitats_demanades
                    select a).ToList();
            }catch(SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return _activitats;
            
        }

        public static List<activitats_demanades> getActivitatsDemanadesByInstalacio(ref string msg, string ins)
        {
            List<activitats_demanades> _activitats = null;

            try
            {
                _activitats = (
                    from a in ORM.db.activitats_demanades
                    where a.espais.instalacions.nom.Contains(ins)
                    select a).ToList();
            }
            catch (SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return _activitats;

        }


        public static List<activitats_demanades> getActivitatsDemanadesByEntitat(ref string msg, string eq)
        {
            List<activitats_demanades> _activitats = null;

            try
            {
                _activitats = (
                    from a in ORM.db.activitats_demanades
                    where a.equips.entitats.nom.Contains(eq)
                    select a).ToList();
            }
            catch (SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return _activitats;

        }

        public static List<activitats_demanades> getActivitatsDemanadesByEquip(int id_equip, ref string msg)
        {
            List<activitats_demanades> _activitats = null;

            try
            {
                _activitats = (
                    from a in ORM.db.activitats_demanades
                    where a.id_equip ==id_equip
                    select a).ToList();
            }
            catch (SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return _activitats;

        }
    }
}
