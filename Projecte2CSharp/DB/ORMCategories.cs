﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projecte2CSharp.DB
{
    public static class ORMCategories
    {

        //SELECT CATEGORIES
        public static List<categories> selectCategories(ref string msg)
        {
            List<categories> _categories = null;

            try
            {
                _categories = (
                from c in ORM.db.categories
                select c).ToList();

            }catch(SqlException e)
            {
                msg = ORM.MensajeError(e);
            }
            

            return _categories;
        }



        //INSERT CATEGORIA
        public static string insertCategoria(categories categoria)
        {
            ORM.db.categories.Add(categoria);
            return ORM.saveChanges();
        }


        //DELETE CATEGORIA
        public static string deleteCategoria(categories categoria)
        {
            ORM.db.categories.Remove(categoria);
            return ORM.saveChanges();
        }


        //UPDATE CATEGORIA
        public static string updateCategoria(categories categoria)
        {
            categories c = ORM.db.categories.Find(categoria.id);
            c = categoria;
            return ORM.saveChanges();
        }
    }
}
