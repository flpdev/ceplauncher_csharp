﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projecte2CSharp.DB
{
    public static class ORMTemporada
    {

        //SELECT ULTIMA TEMPORADA GUARDADA A LA BD
        public static temporades selectUltimaTemporada(ref string msg)
        {
            temporades temporada = null;

            try
            {
                temporada = (
                        from t in ORM.db.temporades
                        orderby t.id descending
                        select t).FirstOrDefault();
            }
            catch (SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return temporada;
        }


        //INSERTAR TEMPORADA
        public static string insertTemporada(temporades temporada)
        {
            ORM.db.temporades.Add(temporada);
            return ORM.saveChanges();
        }
    }
}
