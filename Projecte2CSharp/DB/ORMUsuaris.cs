﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projecte2CSharp.DB
{
    public static class ORMUsuaris
    {

        //SELECT USUARI PER NOM D'USUARI
        public static usuaris selectUsuariByUsername(string username, ref string msg)
        {
            usuaris user = null;

            try
            {
                user = (
                        from u in ORM.db.usuaris
                        where u.correu.Equals(username)
                        select u).FirstOrDefault();
            }
            catch (SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return user;
        }


        //SELECT TOTS ELS USUARIS MENYS EL ACTUAL, PER LA CONFIG
        public static List<usuaris> selectUsuariExceptCurrent(int id, ref string msg)
        {
            List<usuaris> users = null;

            try
            {
                users = (
                        from u in ORM.db.usuaris
                        where u.id != id
                        select u).ToList();
            }
            catch (SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return users;
        }


        
        //UPDATE USUARI
        public static string updateUsuari(usuaris usuari)
        {
            usuaris u = ORM.db.usuaris.Find(usuari.id);
            u = usuari;
            return ORM.saveChanges();
        }

        //DELETE USUARI
        public static string deleteUsuari(usuaris usuari)
        {
            ORM.db.usuaris.Remove(usuari);
            return ORM.saveChanges();
        }


        //INSERT USUARI
        public static string insertUsuari(usuaris usuari)
        {
            ORM.db.usuaris.Add(usuari);
            return ORM.saveChanges();
        }
    }
}
