﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projecte2CSharp.DB
{
    public static class ORMInstalacions
    {

        //SELECT TOTES LES INSTALACIONS
        public static List<instalacions> selectInstalacions(ref string msg)
        {
            List<instalacions> _instalacions = null;

            try
            {
                _instalacions = (
                       from i in ORM.db.instalacions
                       where i.borrat == false
                       orderby i.nom
                       select i).ToList();

                foreach (instalacions i in _instalacions)
                {
                    i.externa_text = i.gestio_externa ? "Si" : "No";
                }
            }
            catch (SqlException e)
            {
                msg = ORM.MensajeError(e);
            }
            
            return _instalacions;
        }


        //SELECT INSTALACIO PER NOM
        public static List<instalacions> selectInstalacionsByNom(string nom, ref string msg)
        {
            List<instalacions> _instalacions = null;

            try
            {
                _instalacions = (
                        from i in ORM.db.instalacions
                        where i.nom.Contains(nom)
                        orderby i.nom
                        select i).ToList();
            }
            catch (SqlException e)
            {
                ORM.MensajeError(e);
            }

            return _instalacions;
        }


        //SELECT INSTALACIO PER ID
        public static instalacions getInsatalacioByID(int id, ref string msg)
        {
            instalacions instalacio = null;
            try
            {
                instalacio = ORM.db.instalacions.Find(id);
            }catch(SqlException e)
            {
                msg = ORM.saveChanges();
            }

            return instalacio;
        }


        //UPDATE INSTALACIO
        public static string updateInstalacio(instalacions instalacio)
        {
            
            instalacions i = ORM.db.instalacions.Find(instalacio.id);
            i = instalacio;

            return ORM.saveChanges();
        }


        //INSERT INSTALACIO
        public static string insertInstalacio(instalacions instalacio)
        {
            ORM.db.instalacions.Add(instalacio);
            return ORM.saveChanges();
        }


        //SOFT-DELETE INSTALACIO
        public static string deleteInstalacio(instalacions instalacio)
        {
            instalacio.borrat = true;
            instalacions i = ORM.db.instalacions.Find(instalacio.id);
            i = instalacio;
            return ORM.saveChanges();
        }

        
    }
}
