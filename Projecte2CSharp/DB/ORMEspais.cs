﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projecte2CSharp.DB
{
    public static class ORMEspais
    {

        //SELECT ESPAIS
        public static List<espais> selectEspais(ref string msg)
        {
            List<espais> _espais = null;
            try
            {
                _espais = (
                        from e in ORM.db.espais
                        where e.borrat == false
                        select e).ToList();
            }
            catch (SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return _espais;
        }

        public static List<espais> selectEspaisByInstalacio(int id_inst, ref string msg)
        {
            List<espais> _espais = null;

            try
            {
                _espais = (
                        from e in ORM.db.espais
                        where e.id_instalacio == id_inst && e.borrat == false
                        select e).ToList();

                foreach (espais espai in _espais)
                {
                    espai.exteriorText = (bool)espai.es_exterior ? "Si" : "No";
                }
            }
            catch (SqlException e)
            {
                msg = ORM.MensajeError(e);               
            }

            return _espais;
        }

        public static List<espais> selectEspaisByNomIInstalacio(int id, string nom, ref string msg)
        {
            List<espais> _espais = null;

            try
            {
                _espais = (
                        from e in ORM.db.espais
                        where e.id_instalacio == id && e.nom.Contains(nom) && e.borrat == false
                        select e).ToList();

                foreach (espais espai in _espais)
                {
                    espai.exteriorText = (bool)espai.es_exterior ? "Si" : "No";
                }
            }
            catch (SqlException e)
            {
                msg = ORM.MensajeError(e);
            }

            return _espais;
        }


        public static espais getEspaiByID(int id, ref string msg)
        {
            espais espai = null;
            try
            {
                espai = ORM.db.espais.Find(id);
            }catch(SqlException e)
            {
                msg = ORM.saveChanges();
            }

            return espai;
        }


        


        public static string updateEspai(espais espai)
        {
            espais e = ORM.db.espais.Find(espai.id);
            e = espai;
            return ORM.saveChanges();
        }

        public static string deleteEspai(espais espai)
        {
            espai.borrat = true;
            espais e = ORM.db.espais.Find(espai.id);
            e = espai;
            return ORM.saveChanges();
        }

        public static string insertEspai(espais espai)
        {
            ORM.db.espais.Add(espai);
            return ORM.saveChanges();
        }
    }
}
