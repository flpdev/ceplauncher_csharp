﻿namespace Projecte2CSharp.UserControls
{
    partial class DemandesControl
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxBusqueda = new System.Windows.Forms.TextBox();
            this.dataGridViewDemandes = new System.Windows.Forms.DataGridView();
            this.nom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.entitat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_equip = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.instalacio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_espai = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.durada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dies = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.es_assignada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonModificar = new System.Windows.Forms.Button();
            this.radioButtonSearchInstalacio = new System.Windows.Forms.RadioButton();
            this.radioButtonSearchEntitat = new System.Windows.Forms.RadioButton();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSourceEquips = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceEspais = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceDemandes = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDemandes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEquips)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEspais)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceDemandes)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 20);
            this.label2.TabIndex = 31;
            this.label2.Text = "Buscar";
            // 
            // textBoxBusqueda
            // 
            this.textBoxBusqueda.Location = new System.Drawing.Point(27, 33);
            this.textBoxBusqueda.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxBusqueda.Name = "textBoxBusqueda";
            this.textBoxBusqueda.Size = new System.Drawing.Size(274, 28);
            this.textBoxBusqueda.TabIndex = 30;
            this.textBoxBusqueda.TextChanged += new System.EventHandler(this.textBoxBusqueda_TextChanged);
            // 
            // dataGridViewDemandes
            // 
            this.dataGridViewDemandes.AllowUserToAddRows = false;
            this.dataGridViewDemandes.AllowUserToDeleteRows = false;
            this.dataGridViewDemandes.AllowUserToResizeColumns = false;
            this.dataGridViewDemandes.AllowUserToResizeRows = false;
            this.dataGridViewDemandes.AutoGenerateColumns = false;
            this.dataGridViewDemandes.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewDemandes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewDemandes.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridViewDemandes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewDemandes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewDemandes.ColumnHeadersHeight = 35;
            this.dataGridViewDemandes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewDemandes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.nom,
            this.entitat,
            this.id_equip,
            this.instalacio,
            this.id_espai,
            this.durada,
            this.tipus,
            this.dies,
            this.es_assignada});
            this.dataGridViewDemandes.DataSource = this.bindingSourceDemandes;
            this.dataGridViewDemandes.EnableHeadersVisualStyles = false;
            this.dataGridViewDemandes.Location = new System.Drawing.Point(27, 147);
            this.dataGridViewDemandes.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dataGridViewDemandes.MultiSelect = false;
            this.dataGridViewDemandes.Name = "dataGridViewDemandes";
            this.dataGridViewDemandes.ReadOnly = true;
            this.dataGridViewDemandes.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewDemandes.RowHeadersVisible = false;
            this.dataGridViewDemandes.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewDemandes.RowTemplate.Height = 35;
            this.dataGridViewDemandes.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewDemandes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridViewDemandes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewDemandes.Size = new System.Drawing.Size(933, 553);
            this.dataGridViewDemandes.TabIndex = 29;
            this.dataGridViewDemandes.DoubleClick += new System.EventHandler(this.dataGridViewDemandes_DoubleClick);
            // 
            // nom
            // 
            this.nom.DataPropertyName = "nom";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.nom.DefaultCellStyle = dataGridViewCellStyle2;
            this.nom.HeaderText = "Nom";
            this.nom.Name = "nom";
            this.nom.ReadOnly = true;
            this.nom.Visible = false;
            // 
            // entitat
            // 
            this.entitat.DataPropertyName = "entitat";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.entitat.DefaultCellStyle = dataGridViewCellStyle3;
            this.entitat.HeaderText = "Entitat";
            this.entitat.Name = "entitat";
            this.entitat.ReadOnly = true;
            this.entitat.Width = 200;
            // 
            // id_equip
            // 
            this.id_equip.DataPropertyName = "id_equip";
            this.id_equip.DataSource = this.bindingSourceEquips;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.id_equip.DefaultCellStyle = dataGridViewCellStyle4;
            this.id_equip.DisplayMember = "nom";
            this.id_equip.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.id_equip.HeaderText = "Equip";
            this.id_equip.Name = "id_equip";
            this.id_equip.ReadOnly = true;
            this.id_equip.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.id_equip.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.id_equip.ValueMember = "id";
            this.id_equip.Width = 150;
            // 
            // instalacio
            // 
            this.instalacio.DataPropertyName = "instalacio";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.instalacio.DefaultCellStyle = dataGridViewCellStyle5;
            this.instalacio.HeaderText = "Instal·lació";
            this.instalacio.Name = "instalacio";
            this.instalacio.ReadOnly = true;
            this.instalacio.Width = 200;
            // 
            // id_espai
            // 
            this.id_espai.DataPropertyName = "id_espai";
            this.id_espai.DataSource = this.bindingSourceEspais;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.id_espai.DefaultCellStyle = dataGridViewCellStyle6;
            this.id_espai.DisplayMember = "nom";
            this.id_espai.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.id_espai.HeaderText = "Espai";
            this.id_espai.Name = "id_espai";
            this.id_espai.ReadOnly = true;
            this.id_espai.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.id_espai.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.id_espai.ValueMember = "id";
            this.id_espai.Visible = false;
            this.id_espai.Width = 150;
            // 
            // durada
            // 
            this.durada.DataPropertyName = "durada";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.durada.DefaultCellStyle = dataGridViewCellStyle7;
            this.durada.HeaderText = "Durada";
            this.durada.Name = "durada";
            this.durada.ReadOnly = true;
            // 
            // tipus
            // 
            this.tipus.DataPropertyName = "tipus";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.tipus.DefaultCellStyle = dataGridViewCellStyle8;
            this.tipus.HeaderText = "Tipus";
            this.tipus.Name = "tipus";
            this.tipus.ReadOnly = true;
            this.tipus.Width = 150;
            // 
            // dies
            // 
            this.dies.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dies.DataPropertyName = "dies";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.dies.DefaultCellStyle = dataGridViewCellStyle9;
            this.dies.HeaderText = "Dies";
            this.dies.Name = "dies";
            this.dies.ReadOnly = true;
            // 
            // es_assignada
            // 
            this.es_assignada.DataPropertyName = "es_assignada";
            this.es_assignada.HeaderText = "es_assignada";
            this.es_assignada.Name = "es_assignada";
            this.es_assignada.ReadOnly = true;
            this.es_assignada.Visible = false;
            // 
            // buttonModificar
            // 
            this.buttonModificar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(182)))));
            this.buttonModificar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonModificar.FlatAppearance.BorderSize = 3;
            this.buttonModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonModificar.Image = global::Projecte2CSharp.Properties.Resources.edit;
            this.buttonModificar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonModificar.Location = new System.Drawing.Point(844, 73);
            this.buttonModificar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.buttonModificar.Name = "buttonModificar";
            this.buttonModificar.Size = new System.Drawing.Size(116, 42);
            this.buttonModificar.TabIndex = 33;
            this.buttonModificar.Text = "Editar";
            this.buttonModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonModificar.UseVisualStyleBackColor = false;
            this.buttonModificar.Click += new System.EventHandler(this.buttonModificar_Click);
            // 
            // radioButtonSearchInstalacio
            // 
            this.radioButtonSearchInstalacio.AutoSize = true;
            this.radioButtonSearchInstalacio.Location = new System.Drawing.Point(48, 82);
            this.radioButtonSearchInstalacio.Name = "radioButtonSearchInstalacio";
            this.radioButtonSearchInstalacio.Size = new System.Drawing.Size(100, 24);
            this.radioButtonSearchInstalacio.TabIndex = 35;
            this.radioButtonSearchInstalacio.TabStop = true;
            this.radioButtonSearchInstalacio.Text = "Instal·lació";
            this.radioButtonSearchInstalacio.UseVisualStyleBackColor = true;
            // 
            // radioButtonSearchEntitat
            // 
            this.radioButtonSearchEntitat.AutoSize = true;
            this.radioButtonSearchEntitat.Location = new System.Drawing.Point(181, 82);
            this.radioButtonSearchEntitat.Name = "radioButtonSearchEntitat";
            this.radioButtonSearchEntitat.Size = new System.Drawing.Size(71, 24);
            this.radioButtonSearchEntitat.TabIndex = 36;
            this.radioButtonSearchEntitat.TabStop = true;
            this.radioButtonSearchEntitat.Text = "Entitat";
            this.radioButtonSearchEntitat.UseVisualStyleBackColor = true;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // bindingSourceEquips
            // 
            this.bindingSourceEquips.DataSource = typeof(Projecte2CSharp.equips);
            // 
            // bindingSourceEspais
            // 
            this.bindingSourceEspais.DataSource = typeof(Projecte2CSharp.espais);
            // 
            // bindingSourceDemandes
            // 
            this.bindingSourceDemandes.DataSource = typeof(Projecte2CSharp.Clases.POJO.ActivitatDemanada);
            // 
            // DemandesControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radioButtonSearchEntitat);
            this.Controls.Add(this.radioButtonSearchInstalacio);
            this.Controls.Add(this.buttonModificar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxBusqueda);
            this.Controls.Add(this.dataGridViewDemandes);
            this.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "DemandesControl";
            this.Size = new System.Drawing.Size(993, 730);
            this.Load += new System.EventHandler(this.DemandesControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDemandes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEquips)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEspais)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceDemandes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxBusqueda;
        private System.Windows.Forms.DataGridView dataGridViewDemandes;
        private System.Windows.Forms.Button buttonModificar;
        private System.Windows.Forms.BindingSource bindingSourceDemandes;
        private System.Windows.Forms.BindingSource bindingSourceEspais;
        private System.Windows.Forms.BindingSource bindingSourceEquips;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nom;
        private System.Windows.Forms.DataGridViewTextBoxColumn entitat;
        private System.Windows.Forms.DataGridViewComboBoxColumn id_equip;
        private System.Windows.Forms.DataGridViewTextBoxColumn instalacio;
        private System.Windows.Forms.DataGridViewComboBoxColumn id_espai;
        private System.Windows.Forms.DataGridViewTextBoxColumn durada;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipus;
        private System.Windows.Forms.DataGridViewTextBoxColumn dies;
        private System.Windows.Forms.DataGridViewTextBoxColumn es_assignada;
        private System.Windows.Forms.RadioButton radioButtonSearchInstalacio;
        private System.Windows.Forms.RadioButton radioButtonSearchEntitat;
    }
}
