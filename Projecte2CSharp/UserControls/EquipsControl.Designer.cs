﻿namespace Projecte2CSharp.UserControls
{
    partial class EquipsControl
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonEliminar = new System.Windows.Forms.Button();
            this.buttonModificar = new System.Windows.Forms.Button();
            this.buttonNou = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxBusqueda = new System.Windows.Forms.TextBox();
            this.dataGridViewEquips = new System.Windows.Forms.DataGridView();
            this.bindingSourceEntitats = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceCategories = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceEsport = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceSexes = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceCompeticions = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceEquips = new System.Windows.Forms.BindingSource(this.components);
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.identitatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.idcategoriaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.idesportDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.idsexeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.idcompeticioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.idcategoriacompeticioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.borratDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.categoriesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoriescompeticioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.competicionsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.entitatsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.esportsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sexesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEquips)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEntitats)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCategories)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEsport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceSexes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCompeticions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEquips)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonEliminar
            // 
            this.buttonEliminar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(181)))));
            this.buttonEliminar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonEliminar.FlatAppearance.BorderSize = 3;
            this.buttonEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEliminar.Image = global::Projecte2CSharp.Properties.Resources.trash;
            this.buttonEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonEliminar.Location = new System.Drawing.Point(844, 86);
            this.buttonEliminar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.buttonEliminar.Name = "buttonEliminar";
            this.buttonEliminar.Size = new System.Drawing.Size(116, 42);
            this.buttonEliminar.TabIndex = 28;
            this.buttonEliminar.Text = "Borrar";
            this.buttonEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonEliminar.UseVisualStyleBackColor = false;
            this.buttonEliminar.Click += new System.EventHandler(this.buttonEliminar_Click);
            // 
            // buttonModificar
            // 
            this.buttonModificar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(182)))));
            this.buttonModificar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonModificar.FlatAppearance.BorderSize = 3;
            this.buttonModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonModificar.Image = global::Projecte2CSharp.Properties.Resources.edit;
            this.buttonModificar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonModificar.Location = new System.Drawing.Point(724, 86);
            this.buttonModificar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.buttonModificar.Name = "buttonModificar";
            this.buttonModificar.Size = new System.Drawing.Size(116, 42);
            this.buttonModificar.TabIndex = 27;
            this.buttonModificar.Text = "Editar";
            this.buttonModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonModificar.UseVisualStyleBackColor = false;
            this.buttonModificar.Click += new System.EventHandler(this.buttonModificar_Click);
            // 
            // buttonNou
            // 
            this.buttonNou.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            this.buttonNou.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(193)))), ((int)(((byte)(66)))));
            this.buttonNou.FlatAppearance.BorderSize = 3;
            this.buttonNou.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNou.Image = global::Projecte2CSharp.Properties.Resources.add;
            this.buttonNou.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonNou.Location = new System.Drawing.Point(27, 86);
            this.buttonNou.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.buttonNou.Name = "buttonNou";
            this.buttonNou.Size = new System.Drawing.Size(274, 42);
            this.buttonNou.TabIndex = 26;
            this.buttonNou.Text = "      Nou equip";
            this.buttonNou.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonNou.UseVisualStyleBackColor = false;
            this.buttonNou.Click += new System.EventHandler(this.buttonNou_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 20);
            this.label2.TabIndex = 25;
            this.label2.Text = "Buscar";
            // 
            // textBoxBusqueda
            // 
            this.textBoxBusqueda.Location = new System.Drawing.Point(27, 33);
            this.textBoxBusqueda.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxBusqueda.Name = "textBoxBusqueda";
            this.textBoxBusqueda.Size = new System.Drawing.Size(274, 28);
            this.textBoxBusqueda.TabIndex = 24;
            this.textBoxBusqueda.TextChanged += new System.EventHandler(this.textBoxBusqueda_TextChanged);
            // 
            // dataGridViewEquips
            // 
            this.dataGridViewEquips.AllowUserToAddRows = false;
            this.dataGridViewEquips.AllowUserToResizeColumns = false;
            this.dataGridViewEquips.AllowUserToResizeRows = false;
            this.dataGridViewEquips.AutoGenerateColumns = false;
            this.dataGridViewEquips.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewEquips.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewEquips.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridViewEquips.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewEquips.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewEquips.ColumnHeadersHeight = 35;
            this.dataGridViewEquips.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewEquips.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.nomDataGridViewTextBoxColumn,
            this.identitatDataGridViewTextBoxColumn,
            this.idcategoriaDataGridViewTextBoxColumn,
            this.idesportDataGridViewTextBoxColumn,
            this.idsexeDataGridViewTextBoxColumn,
            this.idcompeticioDataGridViewTextBoxColumn,
            this.idcategoriacompeticioDataGridViewTextBoxColumn,
            this.borratDataGridViewCheckBoxColumn,
            this.categoriesDataGridViewTextBoxColumn,
            this.categoriescompeticioDataGridViewTextBoxColumn,
            this.competicionsDataGridViewTextBoxColumn,
            this.entitatsDataGridViewTextBoxColumn,
            this.esportsDataGridViewTextBoxColumn,
            this.sexesDataGridViewTextBoxColumn});
            this.dataGridViewEquips.DataSource = this.bindingSourceEquips;
            this.dataGridViewEquips.EnableHeadersVisualStyles = false;
            this.dataGridViewEquips.Location = new System.Drawing.Point(27, 167);
            this.dataGridViewEquips.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dataGridViewEquips.MultiSelect = false;
            this.dataGridViewEquips.Name = "dataGridViewEquips";
            this.dataGridViewEquips.ReadOnly = true;
            this.dataGridViewEquips.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewEquips.RowHeadersVisible = false;
            this.dataGridViewEquips.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewEquips.RowTemplate.Height = 35;
            this.dataGridViewEquips.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewEquips.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridViewEquips.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewEquips.Size = new System.Drawing.Size(933, 533);
            this.dataGridViewEquips.TabIndex = 23;
            this.dataGridViewEquips.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridViewEquips_UserDeletingRow);
            this.dataGridViewEquips.DoubleClick += new System.EventHandler(this.dataGridViewInstalacions_DoubleClick);
            // 
            // bindingSourceEntitats
            // 
            this.bindingSourceEntitats.DataSource = typeof(Projecte2CSharp.entitats);
            // 
            // bindingSourceCategories
            // 
            this.bindingSourceCategories.DataSource = typeof(Projecte2CSharp.categories);
            // 
            // bindingSourceEsport
            // 
            this.bindingSourceEsport.DataSource = typeof(Projecte2CSharp.esports);
            // 
            // bindingSourceSexes
            // 
            this.bindingSourceSexes.DataSource = typeof(Projecte2CSharp.sexes);
            // 
            // bindingSourceCompeticions
            // 
            this.bindingSourceCompeticions.DataSource = typeof(Projecte2CSharp.competicions);
            // 
            // bindingSourceEquips
            // 
            this.bindingSourceEquips.DataSource = typeof(Projecte2CSharp.equips);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // nomDataGridViewTextBoxColumn
            // 
            this.nomDataGridViewTextBoxColumn.DataPropertyName = "nom";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.nomDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.nomDataGridViewTextBoxColumn.HeaderText = "Nom";
            this.nomDataGridViewTextBoxColumn.Name = "nomDataGridViewTextBoxColumn";
            this.nomDataGridViewTextBoxColumn.ReadOnly = true;
            this.nomDataGridViewTextBoxColumn.Width = 150;
            // 
            // identitatDataGridViewTextBoxColumn
            // 
            this.identitatDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.identitatDataGridViewTextBoxColumn.DataPropertyName = "id_entitat";
            this.identitatDataGridViewTextBoxColumn.DataSource = this.bindingSourceEntitats;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.identitatDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.identitatDataGridViewTextBoxColumn.DisplayMember = "nom";
            this.identitatDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.identitatDataGridViewTextBoxColumn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.identitatDataGridViewTextBoxColumn.HeaderText = "Entitat";
            this.identitatDataGridViewTextBoxColumn.Name = "identitatDataGridViewTextBoxColumn";
            this.identitatDataGridViewTextBoxColumn.ReadOnly = true;
            this.identitatDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.identitatDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.identitatDataGridViewTextBoxColumn.ValueMember = "id";
            // 
            // idcategoriaDataGridViewTextBoxColumn
            // 
            this.idcategoriaDataGridViewTextBoxColumn.DataPropertyName = "id_categoria";
            this.idcategoriaDataGridViewTextBoxColumn.DataSource = this.bindingSourceCategories;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.idcategoriaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.idcategoriaDataGridViewTextBoxColumn.DisplayMember = "nom";
            this.idcategoriaDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.idcategoriaDataGridViewTextBoxColumn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.idcategoriaDataGridViewTextBoxColumn.HeaderText = "Categoria";
            this.idcategoriaDataGridViewTextBoxColumn.Name = "idcategoriaDataGridViewTextBoxColumn";
            this.idcategoriaDataGridViewTextBoxColumn.ReadOnly = true;
            this.idcategoriaDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.idcategoriaDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.idcategoriaDataGridViewTextBoxColumn.ValueMember = "id";
            this.idcategoriaDataGridViewTextBoxColumn.Width = 150;
            // 
            // idesportDataGridViewTextBoxColumn
            // 
            this.idesportDataGridViewTextBoxColumn.DataPropertyName = "id_esport";
            this.idesportDataGridViewTextBoxColumn.DataSource = this.bindingSourceEsport;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.idesportDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.idesportDataGridViewTextBoxColumn.DisplayMember = "nom";
            this.idesportDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.idesportDataGridViewTextBoxColumn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.idesportDataGridViewTextBoxColumn.HeaderText = "Esport";
            this.idesportDataGridViewTextBoxColumn.Name = "idesportDataGridViewTextBoxColumn";
            this.idesportDataGridViewTextBoxColumn.ReadOnly = true;
            this.idesportDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.idesportDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.idesportDataGridViewTextBoxColumn.ValueMember = "id";
            this.idesportDataGridViewTextBoxColumn.Width = 150;
            // 
            // idsexeDataGridViewTextBoxColumn
            // 
            this.idsexeDataGridViewTextBoxColumn.DataPropertyName = "id_sexe";
            this.idsexeDataGridViewTextBoxColumn.DataSource = this.bindingSourceSexes;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.idsexeDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this.idsexeDataGridViewTextBoxColumn.DisplayMember = "nom";
            this.idsexeDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.idsexeDataGridViewTextBoxColumn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.idsexeDataGridViewTextBoxColumn.HeaderText = "Sexe";
            this.idsexeDataGridViewTextBoxColumn.Name = "idsexeDataGridViewTextBoxColumn";
            this.idsexeDataGridViewTextBoxColumn.ReadOnly = true;
            this.idsexeDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.idsexeDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.idsexeDataGridViewTextBoxColumn.ValueMember = "id";
            this.idsexeDataGridViewTextBoxColumn.Width = 150;
            // 
            // idcompeticioDataGridViewTextBoxColumn
            // 
            this.idcompeticioDataGridViewTextBoxColumn.DataPropertyName = "id_competicio";
            this.idcompeticioDataGridViewTextBoxColumn.DataSource = this.bindingSourceCompeticions;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(214)))), ((int)(((byte)(138)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.idcompeticioDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this.idcompeticioDataGridViewTextBoxColumn.DisplayMember = "nom";
            this.idcompeticioDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.idcompeticioDataGridViewTextBoxColumn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.idcompeticioDataGridViewTextBoxColumn.HeaderText = "Competició";
            this.idcompeticioDataGridViewTextBoxColumn.Name = "idcompeticioDataGridViewTextBoxColumn";
            this.idcompeticioDataGridViewTextBoxColumn.ReadOnly = true;
            this.idcompeticioDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.idcompeticioDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.idcompeticioDataGridViewTextBoxColumn.ValueMember = "id";
            // 
            // idcategoriacompeticioDataGridViewTextBoxColumn
            // 
            this.idcategoriacompeticioDataGridViewTextBoxColumn.DataPropertyName = "id_categoria_competicio";
            this.idcategoriacompeticioDataGridViewTextBoxColumn.HeaderText = "id_categoria_competicio";
            this.idcategoriacompeticioDataGridViewTextBoxColumn.Name = "idcategoriacompeticioDataGridViewTextBoxColumn";
            this.idcategoriacompeticioDataGridViewTextBoxColumn.ReadOnly = true;
            this.idcategoriacompeticioDataGridViewTextBoxColumn.Visible = false;
            // 
            // borratDataGridViewCheckBoxColumn
            // 
            this.borratDataGridViewCheckBoxColumn.DataPropertyName = "borrat";
            this.borratDataGridViewCheckBoxColumn.HeaderText = "borrat";
            this.borratDataGridViewCheckBoxColumn.Name = "borratDataGridViewCheckBoxColumn";
            this.borratDataGridViewCheckBoxColumn.ReadOnly = true;
            this.borratDataGridViewCheckBoxColumn.Visible = false;
            // 
            // categoriesDataGridViewTextBoxColumn
            // 
            this.categoriesDataGridViewTextBoxColumn.DataPropertyName = "categories";
            this.categoriesDataGridViewTextBoxColumn.HeaderText = "categories";
            this.categoriesDataGridViewTextBoxColumn.Name = "categoriesDataGridViewTextBoxColumn";
            this.categoriesDataGridViewTextBoxColumn.ReadOnly = true;
            this.categoriesDataGridViewTextBoxColumn.Visible = false;
            // 
            // categoriescompeticioDataGridViewTextBoxColumn
            // 
            this.categoriescompeticioDataGridViewTextBoxColumn.DataPropertyName = "categories_competicio";
            this.categoriescompeticioDataGridViewTextBoxColumn.HeaderText = "categories_competicio";
            this.categoriescompeticioDataGridViewTextBoxColumn.Name = "categoriescompeticioDataGridViewTextBoxColumn";
            this.categoriescompeticioDataGridViewTextBoxColumn.ReadOnly = true;
            this.categoriescompeticioDataGridViewTextBoxColumn.Visible = false;
            // 
            // competicionsDataGridViewTextBoxColumn
            // 
            this.competicionsDataGridViewTextBoxColumn.DataPropertyName = "competicions";
            this.competicionsDataGridViewTextBoxColumn.HeaderText = "competicions";
            this.competicionsDataGridViewTextBoxColumn.Name = "competicionsDataGridViewTextBoxColumn";
            this.competicionsDataGridViewTextBoxColumn.ReadOnly = true;
            this.competicionsDataGridViewTextBoxColumn.Visible = false;
            // 
            // entitatsDataGridViewTextBoxColumn
            // 
            this.entitatsDataGridViewTextBoxColumn.DataPropertyName = "entitats";
            this.entitatsDataGridViewTextBoxColumn.HeaderText = "entitats";
            this.entitatsDataGridViewTextBoxColumn.Name = "entitatsDataGridViewTextBoxColumn";
            this.entitatsDataGridViewTextBoxColumn.ReadOnly = true;
            this.entitatsDataGridViewTextBoxColumn.Visible = false;
            // 
            // esportsDataGridViewTextBoxColumn
            // 
            this.esportsDataGridViewTextBoxColumn.DataPropertyName = "esports";
            this.esportsDataGridViewTextBoxColumn.HeaderText = "esports";
            this.esportsDataGridViewTextBoxColumn.Name = "esportsDataGridViewTextBoxColumn";
            this.esportsDataGridViewTextBoxColumn.ReadOnly = true;
            this.esportsDataGridViewTextBoxColumn.Visible = false;
            // 
            // sexesDataGridViewTextBoxColumn
            // 
            this.sexesDataGridViewTextBoxColumn.DataPropertyName = "sexes";
            this.sexesDataGridViewTextBoxColumn.HeaderText = "sexes";
            this.sexesDataGridViewTextBoxColumn.Name = "sexesDataGridViewTextBoxColumn";
            this.sexesDataGridViewTextBoxColumn.ReadOnly = true;
            this.sexesDataGridViewTextBoxColumn.Visible = false;
            // 
            // EquipsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonEliminar);
            this.Controls.Add(this.buttonModificar);
            this.Controls.Add(this.buttonNou);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxBusqueda);
            this.Controls.Add(this.dataGridViewEquips);
            this.Font = new System.Drawing.Font("Source Sans Pro", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "EquipsControl";
            this.Size = new System.Drawing.Size(993, 730);
            this.Load += new System.EventHandler(this.EquipsControl_Load);
            this.Enter += new System.EventHandler(this.EquipsControl_Enter);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEquips)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEntitats)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCategories)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEsport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceSexes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCompeticions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEquips)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonEliminar;
        private System.Windows.Forms.Button buttonModificar;
        private System.Windows.Forms.Button buttonNou;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxBusqueda;
        private System.Windows.Forms.DataGridView dataGridViewEquips;
        private System.Windows.Forms.BindingSource bindingSourceEquips;
        private System.Windows.Forms.BindingSource bindingSourceEntitats;
        private System.Windows.Forms.BindingSource bindingSourceSexes;
        private System.Windows.Forms.BindingSource bindingSourceCategories;
        private System.Windows.Forms.BindingSource bindingSourceEsport;
        private System.Windows.Forms.BindingSource bindingSourceCompeticions;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn identitatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn idcategoriaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn idesportDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn idsexeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn idcompeticioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idcategoriacompeticioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn borratDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoriesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoriescompeticioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn competicionsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn entitatsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn esportsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sexesDataGridViewTextBoxColumn;
    }
}
