﻿using Projecte2CSharp.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projecte2CSharp.Clases.POJO
{
    public class ActivitatEquip
    {
        public int id { get; set; }
        public string nom { get; set; }
        public int id_entitat { get; set; }
        public int id_categoria { get; set; }
        public List<ActivitatConcedida> activitatsConcedides { get; set; } = new List<ActivitatConcedida>();

        public ActivitatEquip(int id)
        {
            this.id = id;
        }

        public List<HorariComplet> getActivitaFromBD()
        {
            
            //agafar instalacio
            string msg = "";
            List<activitats_concedides> _activitats = ORMActivitatConcedida.selectActivitatConcedidaByEquip(id, ref msg).ToList();
            
            List<HorariComplet> horaris = new List<HorariComplet>();
            
            
                foreach(activitats_concedides ac in _activitats)
                {
                    espais espai = ORMEspais.getEspaiByID(ac.id_espai, ref msg);
                    instalacions instalacio = ORMInstalacions.getInsatalacioByID(espai.id_instalacio, ref msg);
                    

                    activitatsConcedides.Add(new ActivitatConcedida(ac.id));
                    foreach (ActivitatConcedida a in activitatsConcedides)
                    {
                        foreach (horaris_activitats act in a.horaris)
                        {
                            dies_setmana dia = ORMDies.getDiaByID(act.id_dia_setmana, ref msg);

                            horaris.Add(new HorariComplet(act.hora_inici, act.hora_final, instalacio.nom, espai.nom, ac.tipus, dia.dia));
                        }
                    }
                }

            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return horaris;
        }

    }

    
}
